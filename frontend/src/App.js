import { h, Component } from "preact";
import Router from "preact-router";
import createHashHistory from "history/createHashHistory";
import axios from "axios";
import classnames from "./helpers/classnames";
import {
  cleanUrl,
  sanitizeCategory,
  gup,
  sanitizeParameterString,
} from "./helpers";
import { defaultFilters, refilterEvents } from "./helpers/filters";
import {
  SearchBar,
  EventsMap,
  EventsPanel,
  EventDetail,
  Loader,
} from "./components";
import "./style.scss";

const addDocumentClass = (className) =>
  (document.documentElement.className =
    document.documentElement.className + ` ${className}`);

if (
  navigator.userAgent.toLowerCase().indexOf("safari") !== -1 &&
  navigator.userAgent.toLowerCase().indexOf("chrome") === -1 &&
  typeof window.ontouchstart === "undefined"
) {
  addDocumentClass("is-safari");
}

class App extends Component {
  state = {
    events: [],
    filteredEvents: [],
    isLoading: false,
    isFiltered: false,
    selectedEvent: false,
    failed: false,
    filters: defaultFilters,
    started: false,
    static_mode: false,
    mapIsLoading: false,
    isAltTitle: false,
  };

  setEvents = (filteredEvents) => {
    this.setState({ filteredEvents });
  };

  toggleFilter = (isFiltered = false) => {
    this.setState({ isFiltered });
  };

  setEvent = (selectedEvent = false) => {
    this.setState({ selectedEvent });
  };

  setFilter = (newFilter = {}) => {
    const { filters, events } = this.state;

    if (newFilter.clear) {
      this.setState({
        filters: defaultFilters,
        filteredEvents: events,
        isFiltered: false,
      });
      document.getElementById("wem-em-category").value = "";
      document.getElementById("wem-em-date").value = "";
    } else {
      const newFilters = { ...filters, ...newFilter };

      const filteredEvents = refilterEvents(events, newFilters);

      const isFiltered = Boolean(
        Object.keys(defaultFilters).find((key) => newFilters[key])
      );

      console.log(
        "setting filters",
        { filters, newFilters },
        `${filteredEvents.length} of ${events.length}`
      );

      this.setState({
        filters: newFilters,
        filteredEvents,
        isFiltered,
      });
    }
    setTimeout(
      () =>
        (document.querySelectorAll(
          ".wem__events-panel__scrollable"
        )[0].scrollTop = 0),
      200
    );
  };

  componentDidMount() {
    const { api_url, api_static, endpoint, alt_title = "" } = this.props;

    const isStatic = Boolean(api_static);

    console.log(`loading app `, { isStatic, endpoint });

    const api_source = isStatic
      ? `${api_static}/map.json`
      : cleanUrl(`${api_url}/${endpoint}/v2/long/`);

    this.setState({
      isLoading: true,
      static_mode: Boolean(api_static),
      isAltTitle: alt_title && gup("alt_title"),
    });
    axios
      .get(api_source)
      .then(({ data }) => (data ? data : Promise.reject("No Data")))
      .then((data) =>
        data && data.length ? data : Promise.reject("No Events")
      )
      .then((data) =>
        data.sort((a, b) =>
          a.high_priority === b.high_priority
            ? a.first_day - b.first_day
            : a.high_priority
            ? -1
            : 1
        )
      )
      .then((data) => {
        this.setState({
          events: data,
          filteredEvents: data,
          mapIsLoading: false,
          isLoading: isStatic,
        });
        addDocumentClass("wem-is-active");
        if (isStatic) {
          const dynamicCategories = {};
          // axios
          //   .get(`${api_static}/some.json`)
          axios
            .get(`${api_static}/all.json`)
            .then(({ data }) => (data ? data : Promise.reject("No Data")))
            .then((data) =>
              data && data.length ? data : Promise.reject("No Events")
            )
            .then((data) => data.sort((a, b) => a.first_day - b.first_day))
            .then((data) => {
              const events = data.map((event) => {
                if (
                  !event.latitude &&
                  event.location &&
                  event.location.location
                ) {
                  event.latitude = event.location.location.latitude;
                  event.longitude = event.location.location.longitude;
                }
                if (event.event_type && !dynamicCategories[event.event_type]) {
                  dynamicCategories[event.event_type] = event.event_type;
                }
                return event;
              });
              this.setState({
                started: true,
                events,
                filteredEvents: events,
                dynamicCategories: Object.keys(dynamicCategories),
              });
              setTimeout(() => this.setState({ isLoading: false }), 100);
            });
        }
        setTimeout(() => this.setState({ started: true }), 100);
      })
      .catch((error) => {
        addDocumentClass("wem-has-failed");
        this.setState({
          started: true,
          failed: error,
          isLoading: false,
          mapIsLoading: false,
        });
      });
  }

  render(props) {
    const {
      api_key,
      api_url,
      base_path,
      title,
      alt_title = "",
      description,
      categories = [],
      mapStyles = [],
      default_bounds,
      show_images = true,
      link_source = "",
      virtual_scroll,
      default_zoom = 8,
      min_zoom = 4,
      no_events_message = "Sorry, no upcoming events at this time.",
      extra_map_features,
      api_static,
    } = this.props;
    const {
      events,
      filteredEvents,
      isFiltered,
      isLoading,
      mapIsLoading,
      selectedEvent,
      isAltTitle,
      filters,
      started,
      failed,
      static_mode,
      dynamicCategories = [],
    } = this.state;

    const categoryOptions = (
      dynamicCategories.length ? dynamicCategories : categories
    ).map((cat) => ({ slug: cat.toUpperCase(), name: sanitizeCategory(cat) }));

    const isVirtual = Boolean(api_static) || Boolean(virtual_scroll);

    const className = classnames(`wem`, {
      "--init": started,
      "--ready": started && !isLoading,
      "--no-events": started && !events.length,
      "--failed": Boolean(failed),
    });

    console.log({
      isLoading,
      started,
      events: events.length,
      filters,
      default_bounds,
    });

    return (
      <div class={className}>
        {!started && <Loader highZIndex={true} />}
        <div class="wem__map-container">
          <SearchBar
            classNames="only-desktop"
            started={started}
            base_path={base_path}
            setFilter={this.setFilter}
            events={events}
            setEvents={this.setEvents}
            setEvent={this.setEvent}
            toggleFilter={this.toggleFilter}
          />
          <EventsMap
            base_path={base_path}
            apiKey={api_key}
            events={filteredEvents}
            isFiltered={isFiltered}
            selectedEvent={selectedEvent}
            setEvent={this.setEvent}
            mapStyles={mapStyles}
            setFilter={this.setFilter}
            filters={filters}
            extra_map_features={extra_map_features}
            isLoading={mapIsLoading}
            default_bounds={default_bounds}
            default_zoom={default_zoom}
            min_zoom={min_zoom}
          />
        </div>

        <Router history={createHashHistory()}>
          <EventsPanel
            path={`/`}
            title={isAltTitle ? alt_title : title}
            description={description}
            events={filteredEvents}
            setEvent={this.setEvent}
            categories={categoryOptions}
            setEvents={this.setEvents}
            toggleFilter={this.toggleFilter}
            setFilter={this.setFilter}
            isFiltered={isFiltered}
            isLoading={isLoading}
            no_events_message={no_events_message}
            show_images={show_images}
            isVirtual={isVirtual}
            link_source={sanitizeParameterString(link_source)}
          />

          <EventDetail
            path={`/:id/:slug`}
            apiUrl={api_url}
            events={events}
            api_static={api_static}
            setEvent={this.setEvent}
            show_images={show_images}
            link_source={sanitizeParameterString(link_source)}
            extra_map_features={extra_map_features}
          />
        </Router>
      </div>
    );
  }
}

export default App;
