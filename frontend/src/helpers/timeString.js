import parse from "date-fns/parse";
import format from "date-fns/format";
/**
 * Given an array of timeslots, display a string describing the events'
 * occurrences over time.
 *
 * @param {Array<Object>} - timeslots
 * @return {String}
 */

const parseUNIX = (timestamp) => new Date(timestamp * 1000);

const timeString = (timeslots) => {
  if (typeof timeslots === "string") {
    return timeslots;
  }
  const dateFormat = "MMMM Do";

  if (timeslots && timeslots.length === 1) {
    const { start_date, end_date } = timeslots[0];
    const startDate = format(parseUNIX(start_date), dateFormat);
    const endDate = format(parseUNIX(end_date), dateFormat);

    if (startDate === endDate) {
      return startDate;
    } else {
      return `${startDate} - ${endDate}`;
    }
  } else if (timeslots && timeslots.length > 1) {
    const { start_date } = timeslots[0];
    const { end_date } = timeslots[timeslots.length - 1];
    const startDate = format(parseUNIX(start_date), dateFormat);
    const endDate = format(parseUNIX(end_date), dateFormat);
    if (startDate === endDate) {
      return `${timeslots.length} shifts on ${startDate}`;
    }
    return `${timeslots.length} shifts between ${startDate} - ${endDate}`;
  }
};

export default timeString;
