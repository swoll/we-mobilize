/**
 * Given a location and sponsor object, outputs a string that represents both
 * sets of data.
 *
 * @param {Object} - { location<Object>, sponsor<Object> }
 * @return {String}
 */
const eventMeta = ({ location, sponsor={} }) => {

  if(!location){
    return '';
  }

  const { candidate_name ='', name='' } = sponsor;

  let output = [];

  //console.log({location})

  if (typeof location.venue !== 'undefined' && location.venue.length) {
    output.push(location.venue);
  } else {
    const { address_lines, locality, region, full_address } = location;
    if(full_address){
      output.push(full_address);
    }else if(address_lines && address_lines.length && address_lines.join("")) {
      const address = address_lines.filter((a) => a.trim() !== '');
      output.push(`${address.concat(locality, region).join(', ')}`);
    }
  }

  if (candidate_name && candidate_name.length || name && name.length) {
    output.push(`Hosted by ${candidate_name||name}`);
  }

  return output.length && output.length > 1 ? output.join(' / ') :  output[0];
};

export const cityState = ({ location={} }) => {

  const {locality, region, postal_code} = location;

  if(locality && (region||postal_code)){
    return <span>{locality}, {region||postal_code}</span>
  }
};




export const cityStateHost = ({ location={}, sponsor={} }) => {

  if(!location){
    return '';
  }

  const {locality, region, postal_code} = location;
  const { candidate_name ='', name='' } = sponsor;
  let host = ''


  if (candidate_name && candidate_name.length || name && name.length) {
    host = `Hosted by ${candidate_name||name}`;
  }

  if(locality && (region||postal_code)){
    return <span>{locality}, {region||postal_code} {host?`/ ${host}`:''}</span>
  }
};


export default eventMeta;
