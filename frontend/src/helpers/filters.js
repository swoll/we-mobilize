import format from "date-fns/format";
import parse from "date-fns/parse";
import haversine from "haversine";

const parseUNIX = (timestamp) => new Date(timestamp * 1000);

const parseDateStamp = (datestamp) => {
  const [year, month, day] = datestamp.split(/\D/);
  return new Date(year, month - 1, day);
};
const dateFormat = "YYYYDDDD";

const parseDateStampToDay = (datestamp) =>
  (datestamp && format(parseDateStamp(datestamp), dateFormat)) || "";

const isCategory = (category) => (event) => category === event.event_type;

const isVirtualOnly = (event) => event.is_virtual;

const virtualAndInPerson = (event) => !event.is_virtual | event.is_virtual;

const _isOnOrAfter = (YYYYDDDD) => (event) =>
  format(parseUNIX(event.last_day || event.first_day), dateFormat) >= YYYYDDDD; //in progress, but idea is that we sort into dates, then can by distance

const isOnOrAfter = (datestamp) => _isOnOrAfter(parseDateStampToDay(datestamp));

const getDistanceFromOrigin =
  (origin, threshold = 500) =>
  (events) =>
    events.reduce((acc, event) => {
      const distanceFromSearch = haversine(origin, event.location.location, {
        unit: "mile",
      });
      event.distanceFromSearch = distanceFromSearch;
      if (distanceFromSearch < threshold) {
        acc.push(event);
      }
      return acc;
    }, []);

//distance
const byDistance = (events) =>
  events.sort((a, b) => a.distanceFromSearch - b.distanceFromSearch);

//sorts by day, then distance within day
const byDayThenDistance = (events) =>
  events.sort(
    (a, b) =>
      format(parseUNIX(a.last_day || a.first_day), dateFormat) -
        format(parseUNIX(b.last_day || b.first_day), dateFormat) ||
      a.distanceFromSearch - b.distanceFromSearch
  );

export const defaultFilters = {
  fromOrigin: false,
  fromDay: false,
  byCategory: false,
  virtualOnly: false,
};

export const refilterEvents = (
  events,
  { fromOrigin, fromDay, byCategory, virtualOnly } // remove virtualOnly
) => {
  let tempevents = [];

  console.log("filters", events, {
    fromOrigin,
    fromDay,
    byCategory,
    virtualOnly,
  });

  //filters
  if (byCategory && fromDay) {
    tempevents = events.filter(
      (x) => isCategory(byCategory)(x) && isOnOrAfter(fromDay)(x)
    );
  } else if (fromDay) {
    tempevents = events.filter(isOnOrAfter(fromDay));
  } else if (byCategory) {
    tempevents = events.filter(isCategory(byCategory));
  } else {
    tempevents = [...events]; //just clone all events
  }

  console.log(`${tempevents.filter(isVirtualOnly).length} vs ${events.length}`);
  return virtualOnly
    ? tempevents.filter(isVirtualOnly)
    : tempevents.filter(virtualAndInPerson);
};
