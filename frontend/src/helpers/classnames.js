var hasOwn = {}.hasOwnProperty;

export default (...classnames) => {
    const classes = [];

    for (let x of classnames){
        const type = typeof x;
        if(type==='string' && x!==''){
            classes.push(x)
        }
        if(type==='object'){
            for (var key in x) {
              if (hasOwn.call(x, key) && x[key]) {
                  classes.push(key);
              }
            }
        }
    }
    return classes.join(' ');
}