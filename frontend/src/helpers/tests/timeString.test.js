import timeString from '../timeString';

describe('timeString', () => {
  it('should print a date for one shift', () => {
    const timeslots = [
      {
        start_date: 1530466200,
        end_date: 1530473400,
      },
    ];

    const result = timeString(timeslots);

    expect(result).toEqual('Jul 1st');
  });

  it('should print a range for one shift', () => {
    const timeslots = [
      {
        start_date: 1530466200,
        end_date: 1530673400,
      },
    ];

    const result = timeString(timeslots);

    expect(result).toEqual('Jul 1st - Jul 3rd');
  });

  it('should print a range multiple shifts', () => {
    const timeslots = [
      {
        start_date: 1530566200,
        end_date: 1530573400,
      },
      {
        start_date: 1530666200,
        end_date: 1530673400,
      },
    ];

    const result = timeString(timeslots);

    expect(result).toEqual('2 shifts between Jul 2nd - Jul 3rd');
  });
});
