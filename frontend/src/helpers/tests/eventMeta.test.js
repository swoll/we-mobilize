import eventMeta from '../eventMeta';

describe('eventMeta', () => {
  it('should print the address', () => {
    const location = {
      address_lines: ['123 Test Lane', ''],
      locality: 'Yorktown',
      region: 'VA',
    };

    const result = eventMeta({ location, sponsor: {} });

    expect(result).toEqual('123 Test Lane, Yorktown, VA');
  });

  it('should print the venue if it exists', () => {
    const location = { venue: 'Venue' };

    const result = eventMeta({ location, sponsor: {} });

    expect(result).toEqual('Venue');
  });

  it('should print the sponsor if it exists', () => {
    const location = { venue: 'Venue' };
    const sponsor = { candidate_name: 'Sponsor' };

    const result = eventMeta({ location, sponsor });

    expect(result).toEqual('Venue / Hosted by Sponsor');
  });
});
