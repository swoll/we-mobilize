import { h, Component } from 'preact';
import timeString from './timeString';
import eventMeta, { cityState, cityStateHost } from './eventMeta';

export const cleanUrl = string => String(string).replace(/(https?:\/\/)|(\/)+/g, '$1$2');

export const cleanEntities = string => string
    .replace( /\u2018|\ufffd|\u201b/g, '&lsquo;' )
    .replace(/\u2013/g, String.fromCharCode(8211))
    .replace(/u2019/g, String.fromCharCode(8217))
    .replace( /\u2019/g, String.fromCharCode(8217) )
    .replace( /\u201a/g, '&sbquo;' )
    .replace( /\u201c/g, '&ldquo;' )
    .replace( /\u201d/g, '&rdquo;' )
    .replace( /&amp;/g, String.fromCharCode( 38 ) )
    .replace( /&#038;/g, String.fromCharCode( 38 ) )
    .replace(/&#8217;/g, String.fromCharCode(8217))
    .replace(/&ndash;/g, String.fromCharCode(8211))
    .replace(/\u2014/g, String.fromCharCode(8212))
    .replace(/&mdash;/g, String.fromCharCode(8212))
    .replace(/\u2026/g, String.fromCharCode(8230))

export const splitLines = (string='') => string.split('\n\n').map((item, key) => <span key={key}>{item}<br/><br/></span>);

export const cleanAndSplitString = string => splitLines(cleanEntities(string));

if (!String.prototype.trim) {
  String.prototype.trim = function () {
    return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
  };
}

export const sanitizeParameterString = (str='') => String(str).replace(/\?/g,'').split('&').map(
    str => str.split('=').map(encodeURIComponent).join('=')
).join('&');

export const appendParameterString = (href, parameters) => parameters ?
    href + (href.indexOf('?')>-1 ? '&' : '?') + parameters :
    href;

export const connectAddThis = () => {
    setTimeout(()=>{
        if(window.addthis && typeof window.addthis.toolbox === "function"){
          window.addthis.toolbox('.wem__event-actions__social');
        }
    }, 100)

};

//gup :: String -> String
export function gup(name) {
    name = String(name).replace(/(\[|\])/g,"\\$1");
    var regex = new RegExp("[\\?&]"+name+"=([^&#]*)"),
        results = regex.exec( window.location.href );
    return ( results === null )?"":results[1];
}

export const scrollToWidgetTop = () => {
    const $maybeAlert = document.querySelector('.ok-to-alert .alert-bar');
    const $maybeTop = document.querySelector('.top');
    const $widget = document.querySelector('[data-widget-host="we-mobilize"]');
    let headerHeight = 0;

    const rect = $widget.getBoundingClientRect();

    if($maybeTop &&  window.getComputedStyle){
        headerHeight = window.getComputedStyle($maybeTop).position === 'sticky' ? -75 : 0;
    }

    //const targetY = window.scrollY + rect.top - (window.innerHeight - $widget.offsetHeight);
    // window.scroll(0, targetY - (
    //   $maybeHeader && ($maybeHeader.offsetHeight + $maybeHeader.offsetTop)||0)
    // );
    //console.log(headerHeight + ($maybeAlert ? $maybeAlert.offsetHeight : 0))
    //scroll internal widget
    window.scroll(0, headerHeight + ($maybeAlert ? $maybeAlert.offsetHeight : 0));//scroll window
}

const capitalize = string => String(string).charAt(0).toUpperCase() + String(string).slice(1).toLowerCase();

export const sanitizeCategory = string => String(string).replace(/_/g,' ').split(' ').map(capitalize).join(' ');

export const slugify = string => string ?
    String(string)
        .trim()
        .replace(/\s/g,'-')// turn spaces into slugs
        .replace(/[^\w-]+/g,'')// remove non-letters/dashes
        .replace(/--+$/, "-")// remove trailing --
        .toLowerCase() :
    '';

export { timeString, eventMeta, cityState, cityStateHost };
