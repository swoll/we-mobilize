const poly = require('preact-cli/lib/lib/webpack/polyfills');

import { h, Component } from 'preact';
import habitat from 'preact-habitat';
import App from './App';

const _habitat = habitat(App);

_habitat.render({
  selector: '[data-widget-host="we-mobilize"]',
  clean: true,
});
