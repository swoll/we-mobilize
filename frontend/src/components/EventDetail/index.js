import { h, Component } from "preact";
import { route } from "preact-router";
import axios from "axios";
import {
  timeString,
  eventMeta,
  cleanAndSplitString,
  cleanEntities,
  scrollToWidgetTop,
  connectAddThis,
  appendParameterString,
} from "../../helpers";
import { CloseButton } from "../Icons/Icons";
import EventActions from "../EventActions/EventActions";
import Loader from "../Loader";
import { PlainStar } from "../Icons/Icons";

const EventDetail = (props) => {
  const {
    base_path,
    browser_url,
    description = "",
    sponsor,
    timeslots,
    title,
    onClose,
    high_priority,
    featured_image_url,
    is_virtual = false,
    location,
    is_wp_item,
    venue = "",
    link_source = "",
    id,
  } = props;

  const {
    full_address = "",
    address_lines = [],
    locality,
    region,
    postal_code,
    venue: loc_venue = "",
  } = location || {};

  const maybeVenue = (venue || loc_venue)
    .replace(/(\r\n|\n|\r)/gm, " ")
    .replace(/\s+/g, " ");

  const actions = {
    ...props,
    isDetailCard: true,
  };
  const address = address_lines.filter((a) => a.trim() !== "");
  const href = appendParameterString(browser_url, link_source);
  return (
    <div class="wem__events-right wem__event wem__event-detail" data-mid={id}>
      <button class="wem__event-detail__close" onClick={onClose}>
        <CloseButton />
      </button>

      <h4 class="wem__event-detail__date">
        {is_virtual && <span class="">Virtual Event &mdash; </span>}
        {timeString(timeslots)}
      </h4>
      {high_priority && (
        <div class="wem_event-card__priority-icon" title="Highest Priority">
          <PlainStar />
        </div>
      )}
      <h2 class="wem__event-detail__title">{cleanEntities(title)}</h2>
      <strong class="wem__event-detail__meta">
        {eventMeta({ location, sponsor })}
      </strong>
      <EventActions {...actions} />

      {featured_image_url ? (
        <figure class="wem__event-detail__image">
          <img src={featured_image_url} />
        </figure>
      ) : (
        <hr />
      )}

      {location && (full_address || postal_code) && (
        <div class="wem__event-detail__content">
          <h4 class="wem__event-detail__headline">Where</h4>
          {full_address && full_address !== maybeVenue && (
            <div>{full_address}</div>
          )}
          {!full_address && (
            <div>
              {!!address.length && address.concat().join(", ")}
              {(!!locality && !!region && !!postal_code && (
                <div>
                  {locality}, {region} {postal_code}
                </div>
              )) ||
                postal_code}
            </div>
          )}
        </div>
      )}

      <div class="wem__event-detail__content">
        <h4 class="wem__event-detail__headline">Description</h4>
        <p class="wem__event-detail__paragraph">
          {cleanAndSplitString(description)}
        </p>
        {location === null && !is_wp_item && (
          <p>
            This event’s address is private.{" "}
            <a href={href} target={!is_wp_item ? "_blank" : null}>
              Sign up for more details.
            </a>
          </p>
        )}
      </div>
    </div>
  );
};

const POIDetail = (props) => {
  const {
    base_path,
    browser_url,
    description,
    location,
    sponsor = "",
    title,
    onClose,
    office_phone_number,
    excerpt,
    featured_image_url,
    location: { full_address = "", address_lines = [] },
  } = props;
  const { locality, region, postal_code } = props.location;
  const actions = {
    ...props,
    nosocial: false,
    isDetailCard: true,
  };
  const address = address_lines.filter((a) => a.trim() !== "");

  return (
    <div class="wem__events-right wem__event wem__event-detail">
      <button class="wem__event-detail__close" onClick={onClose}>
        <CloseButton />
      </button>

      <h2 class="wem__event-detail__title">{title}</h2>
      {excerpt && <p>{excerpt}</p>}

      {featured_image_url ? (
        <figure class="wem__event-detail__image">
          <img src={featured_image_url} />
        </figure>
      ) : (
        <hr />
      )}

      <div class="wem__event-detail__content">
        <h4 class="wem__event-detail__headline">Where</h4>
        {sponsor &&
          sponsor.candidate_name.length > 0 &&
          false && [
            <p class="wem__event-detail__paragraph">
              {sponsor.candidate_name}
            </p>,
          ]}
        {full_address || (
          <span>
            {address.join(", ")}
            {locality && region && postal_code && (
              <div>
                {location.locality}, {location.region} {location.postal_code}
              </div>
            )}
          </span>
        )}

        {office_phone_number && (
          <div class="wem__event-detail__phone">{office_phone_number}</div>
        )}
      </div>

      <div class="wem__event-detail__content">
        <h4 class="wem__event-detail__headline">Details</h4>
        <div
          class="wem__event-detail__paragraph"
          dangerouslySetInnerHTML={{ __html: description }}
        ></div>
      </div>
    </div>
  );
};

class ItemDetail extends Component {
  state = { event: null };

  onClose = (e) => {
    const { base_path, setEvent } = this.props;
    setEvent();
    route("");
    scrollToWidgetTop();
  };

  componentDidMount() {
    const {
      apiUrl,
      id,
      base_path,
      setEvent,
      api_static,
      events,
      extra_map_features,
    } = this.props;

    // const endpoint = id.indexOf('event')>-1 ?
    //   `${apiUrl}/we-mobilize/v2/wp-event/?post_id=${String(id).replace('event-','')}` :
    //   `${apiUrl}/we-mobilize/v2/event/?post_id=${id}`;

    const hasEvent =
      events &&
      events.length &&
      events.find((event) => event.id === id && event.location);
    const hasPoi =
      extra_map_features &&
      extra_map_features.length &&
      extra_map_features.find((poi) => String(poi.post_id) === String(id));

    console.log({ extra_map_features, id, hasPoi, hasEvent });

    if (hasEvent || hasPoi) {
      console.log(`pre-loaded event`);
      this.setState({ event: hasEvent || hasPoi });
      setEvent(hasEvent || hasPoi);
      connectAddThis();
    } else {
      const endpoint = api_static
        ? `${api_static}/events/${id}.json`
        : `${apiUrl}/we-mobilize/v2/event/?post_id=${id}`;
      //console.log('event detail',{endpoint, id});
      axios
        .get(endpoint)
        .then((response) => {
          this.setState({ event: response.data });
          setEvent(response.data);
          connectAddThis();
        })
        .catch((error) => {
          console.error(error);
          setEvent();
          route("");
          scrollToWidgetTop();
        });
    }

    scrollToWidgetTop();
  }

  render(props, state) {
    const { base_path } = props;

    if (state.event) {
      const { is_poi } = state.event;

      const mergeProps = {
        ...state.event,
        base_path,
        onClose: this.onClose.bind(this),
      };

      return is_poi ? (
        <POIDetail {...mergeProps} />
      ) : (
        <EventDetail {...mergeProps} />
      );
    } else {
      return (
        <div class="wem__events-right wem__event-detail">
          <Loader />
        </div>
      );
    }
  }
}

export default ItemDetail;
