import { h, Component } from 'preact';
import classnames from '../../helpers/classnames';

// class Loader extends Component {
//   render() {
//     return (
//       <div class="wem__loader">
//         <i class="wem__loader__icon fa fas fa-spinner" />
//       </div>
//     );
//   }
// }

class Loader extends Component {
  render(props) {
    const { highZIndex, style } = props;
    const className = classnames("wem__loader", { high: highZIndex });
    return (
      <div class={className} style={style}>
        <div class="wem__loader__icon">
          <div style="width:100%;height:100%" class="lds-dual-ring"></div>
        </div>
      </div>
    );
  }
}


export default Loader;
