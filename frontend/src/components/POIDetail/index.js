import { h, Component } from 'preact';
import { route } from 'preact-router';
import axios from 'axios';
import { timeString, eventMeta } from '../../helpers';
import { CloseButton } from '../Icons/Icons';
import EventActions from '../EventActions/EventActions';
import Loader from '../Loader';

class POIDetail extends Component {
  state = { event: null };

  onClose = (e) => {
    const { base_path, setEvent } = this.props;
    setEvent();
    route( base_path );
  };

  componentDidMount() {
    const { apiUrl, id, base_path } = this.props;

    const endpoint = id.indexOf('event')>-1 ?
      `${apiUrl}/we-mobilize/v2/wp-event/?post_id=${String(id).replace('event-','')}` :
      `${apiUrl}/we-mobilize/v2/event/?post_id=${id}`;

    axios
      .get(endpoint)
      .then((response) => {
        this.setState({ event: response.data });
      })
      .catch((error) => {
        console.error(error);
        route( base_path );//couldn't find event, so load map
      });
  }

  render(props, state) {
    const { base_path } = props;

    if (state.event) {
      const {
        browser_url,
        description,
        location,
        sponsor,
        timeslots,
        title,
        featured_image_url,
        office_phone_number,
        location : {
          full_address = '',
          address_lines = []
        }
      } = state.event;

      const actions = {
        ...state.event,
        isDetailCard: true
      }

      console.log({description})

      const address = address_lines.filter((a) => a.trim() !== '');

      return (
        <div class="wem__event wem__event-detail">
          <button class="wem__event-detail__close" onClick={this.onClose}>
            <CloseButton/>
          </button>

          <h4 class="wem__event-detail__date">{timeString(timeslots)}</h4>
          <h2 class="wem__event-detail__title">{title}</h2>
          <strong class="wem__event-detail__meta">{eventMeta({ location, sponsor })}</strong>
          <EventActions {...actions}/>

          { featured_image_url ?
              (<figure class="wem__event-detail__image"><img src={featured_image_url}/></figure>) :
              (<hr/>)
          }

          <div class="wem__event-detail__content">
            <h4 class="wem__event-detail__headline">Where</h4>
            {
              sponsor && sponsor.candidate_name.length > 0 &&
              [
                <p class="wem__event-detail__paragraph">{sponsor.candidate_name}</p>,
                <br />,
              ]
            }
            { full_address || (
              <span>
                {address.join(', ')}
                <br />
                {location.locality}, {location.region} {location.postal_code}
                </span>
              )
            }
            {
              office_phone_number && <div class="wem__event-detail__phone">{office_phone_number}</div>
            }
          </div>

          {
            description && (
            <div class="wem__event-detail__content">
              <h4 class="wem__event-detail__headline">Description</h4>
              <p class="wem__event-detail__paragraph">{description}</p>
            </div>
            )
          }
        </div>
      );
    } else {
      return (
        <div class="wem__event-detail">
          <Loader />
        </div>
      );
    }
  }
}

export default POIDetail;
