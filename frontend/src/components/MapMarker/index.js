import { h, Component } from 'preact';
import { getCurrentUrl, route } from 'preact-router';
import classnames from '../../helpers/classnames';
import { cleanUrl, slugify, scrollToWidgetTop } from '../../helpers';

class MapMarker extends Component {
  onClick = () => {
    const { base_path, id, post_id, is_wp_item, setThisEvent, title, type = 'event' } = this.props;

    // Couldn't get routing to play nice with just the second route call. This
    // needs to be figured out down the road.

    const ev_id = post_id||id;

    const newroute = cleanUrl(`/${ev_id}/${slugify(title)}`);
    route(`/`);
    setThisEvent();
    route(newroute);
    //console.log(newroute);
    scrollToWidgetTop();
  };

  render(props) {
    const { id, post_id, selectedEventId, title, Icon, noshadow } = props;

    const ev_id = post_id||id;

    const className = classnames( Icon ? 'wem__events-map__icon' : 'wem__events-map__marker', {
      'is-active': selectedEventId === Number(ev_id),
      noshadow
    });

    return (
      <div id={`marker-${ev_id}`} title={title} class={className} onClick={this.onClick} >{Icon && <Icon/>}</div>
    );
  }
}

export default MapMarker;
