import { h, Component } from "preact";
import { Caret } from "../Icons/Icons";
import classnames from "../../helpers/classnames";
import { appendParameterString } from "../../helpers";

export const CardAction = ({
  browser_url,
  local_url,
  is_wp_item,
  children,
  isDetailCard,
  title,
  has_rsvp_url,
  link_source = "",
}) => {
  let type = "rsvp";
  if (!browser_url) {
    return null;
  }
  if (is_wp_item && ((!local_url && !browser_url) || !has_rsvp_url)) {
    return null;
  }
  if (children && String(children[0]).toLowerCase() !== "rsvp") {
    type = "details";
    if (isDetailCard && is_wp_item) {
      return null;
    }
  }
  const href = appendParameterString(
    type === "details" && local_url ? local_url : browser_url,
    link_source
  );
  return (
    <a
      onClick={(e) => e.stopPropagation()}
      class={`wem__event-actions__url wem__event-actions__url-${type}`}
      href={href}
      target={!is_wp_item ? "_blank" : null}
    >
      <span>{children} </span>
    </a>
  );
};

export const CardSocial = ({ browser_url, local_url, title }) => {
  const encoded_url = encodeURIComponent(browser_url);
  const encoded_title = encodeURIComponent(title);

  const emailProps = {
    "addthis:title": title,
    "addthis:url": browser_url,
    "addthis:ui_email_note": "Note",
    target: "blank",
    title: "Email",
    href: `mailto:?subject=${encoded_title}&body=${encoded_url}`,
    onClick: (e) => {
      if (window.addthis && window.addthis_config) {
        addthis_config.ui_email_note = browser_url;
      }
    },
  };

  return (
    <div class="wem__event-actions__social">
      <a
        class="wem__event-actions__social-link social-pop"
        href={`http://www.facebook.com/sharer.php?u=${encoded_url}`}
      >
        <i class="fa fab fa-facebook-f" target="_blank" />
      </a>
      <a
        class="wem__event-actions__social-link social-pop"
        href={`http://twitter.com/intent/tweet?text=${encoded_title}&url=${encoded_url}`}
        target="_blank"
      >
        <i class="fa fab fa-twitter" />
      </a>
      <a
        class="wem__event-actions__social-link addthis_button_email"
        {...emailProps}
      >
        <i class="fa fas fa-envelope" />
      </a>
    </div>
  );
};

const EventActions = (eventprops) => {
  const { nosocial, has_rsvp_url, is_wp_item, browser_url, local_url } =
    eventprops;

  const className = classnames("wem__event-actions", {
    "--no-url": is_wp_item && ((!local_url && !browser_url) || !has_rsvp_url),
  });

  return (
    <div class={className}>
      <CardAction {...eventprops}>Event Details</CardAction>
      <CardAction {...eventprops}>RSVP</CardAction>
      {!nosocial && <CardSocial {...eventprops} />}
    </div>
  );
};

export default EventActions;
