import { h, Component } from 'preact';
import { route } from 'preact-router';
import { timeString, eventMeta, cleanEntities, scrollToWidgetTop, cityStateHost } from '../../helpers';
import EventActions from '../EventActions/EventActions';
import { PlainStar } from '../Icons/Icons';

import { cleanUrl, slugify } from '../../helpers';

const truncate = n => string => <span>{ string.substr(0,n-1).trim() }{string.length>n?<span>&hellip;</span>:''}</span>;

class EventCard extends Component {
  onClick = (e) => {
    const { base_path, event, setEvent } = this.props;
    const { id, post_id, slug, is_wp_item, title } = event;
    const newroute = cleanUrl(`${post_id||id}/${slugify(title)}`);
    setEvent(event);
    route( newroute );
    scrollToWidgetTop();
  };

  render(props) {
    const { event, show_images, isVirtual, itemHeight = 290, link_source='' } = props;
    const {
      browser_url = '',
      location,
      sponsor,
      summary = '',
      timeslots,
      title,
      description = '',
      is_virtual = false,
      post_id,
      id,
      is_wp_item,
      high_priority = false,
      featured_image_url,
      priority = false
    } = event;

    const className = `wem__event wem__event-card${show_images && featured_image_url ?' --has-image':''}`

    const hasImage = show_images && featured_image_url;

    const cleanTitle = cleanEntities(title);

    const isLongTitle = cleanTitle.length > 70;

    const titleClass = `wem__event-card__title h5${isLongTitle ?' wem_long_title':''}`

    const descriptionLimit = isVirtual && hasImage ?
      (cleanTitle.length>=39 ? 85 : 130) :
      140;

    const descriptionLimitShort = isVirtual && hasImage ?
      (cleanTitle.length>=39 ? 65 : 90) :
      108;

    return (
      <div class={className} onClick={this.onClick} id={post_id||id} style={{height:itemHeight}}>
        <div class="wem__event-card__details">
          
          <h6 class="wem__event-card__date h6">{ is_virtual && <span class="">Virtual Event &mdash; </span> }{timeString(timeslots)}</h6>
          { high_priority && <div class="wem_event-card__priority-icon" title="Highest Priority"><PlainStar/></div>}
          <h3 class={titleClass}>{cleanTitle}</h3>
          <strong class="wem__event-card__meta city-state">{cityStateHost({location, sponsor })}</strong>
          {(summary || description) && <p class="wem__event-card__description description-short">{truncate(descriptionLimitShort)(cleanEntities(summary || description))}</p> }
          {(summary || description) && <p class="wem__event-card__description description-long">{truncate(descriptionLimit)(cleanEntities(summary || description))}</p> }

          <EventActions link_source={link_source} {...event}/>

        </div>
        { show_images && featured_image_url && <figure class="wem__event-card__image"><img src={featured_image_url}/></figure> }
      </div>
    );
  }
}

export default EventCard;
