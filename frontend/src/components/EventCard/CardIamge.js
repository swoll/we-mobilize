import { h, Component } from 'preact';
import classnames from './helpers/classnames';

const testImgStyles = {
  height: 'auto',
  minHeight: '0 !important',
  minWidth: '0 !important',
  maxWidth: 'none !important',
  position: 'absolute',
  visibility: 'hidden',
  width: '100px !important'
};

class RepImage extends Component {
    constructor (){
        super();
        this.state = {
            imgloaded: false,
            imgerror: false,
            classes: []
        };
        this.handleImageLoad = this.handleImageLoad.bind(this);
        this.handleImageError = this.handleImageError.bind(this);
    }
    handleImageLoad({target: {offsetHeight = 0, offsetWidth = 0 } = {} } = {}){

      const classes = [];
      //console.log({offsetHeight, offsetWidth, ratio: offsetHeight/offsetWidth}, this.props.name, this.props.imgsrc);
      if(offsetHeight > (offsetWidth * 1.3)){
        classes.push('tall-img');
      }
      if(offsetHeight > (offsetWidth * 1.35)){
        classes.push('very-tall-img');
      }
      if(offsetWidth > (offsetHeight * 1.2)){
        classes.push('wide-img');
      }
      if(offsetWidth > (offsetHeight * 1.5)){
        classes.push('super-wide-img');
      }
      if(this.props.imgsrc.indexOf('data:image/svg+xml')>-1){
        classes.push('fallback-img');
      }
      if(this.p){
        classes.push('wide-img');
      }


      this.setState({
        imgloaded: true,
        classes,
        offsetHeight,
        offsetWidth,
        ratio: offsetHeight/offsetWidth
      });
    }
    handleImageError(){
      this.setState({
        imgerror: true
      });
    }
    render () {
        const { imgsrc, figureStyles, name } = this.props;
        const { imgloaded, imgerror, classes } = this.state;
        const { handleImageLoad, handleImageError } = this;

        const showImage = imgsrc && !imgerror && imgloaded;
        const showTestImg = !!imgsrc && !imgerror && !imgloaded;

        const figureClasses = classnames(...classes, { imgerror, imgloaded });

        return (
            <figure style={figureStyles} className={figureClasses}>
            {
              showImage && (
                <img className="realimg" src={imgsrc} />
              )
            }
            {
              showTestImg && (
                <img className="testimg" src={imgsrc} onLoad={handleImageLoad} onError={handleImageError} style={testImgStyles} />
              )
            }
            </figure>
        )
    }
}

export default RepImage;