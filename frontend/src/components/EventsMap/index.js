import { h, Component } from "preact";
import MapMarker from "../MapMarker";
import GoogleMapReact from "google-map-react";
import { fitBounds } from "google-map-react/utils";
import { Star } from "../Icons/Icons";
import Loader from "../Loader";
import supercluster from "points-cluster";

const maxZoom = 15;
const clusterRadius = 60;

// this function merges external props in with those
// that might need access to the maps prop Maps passes in
const createMapOptions = (extraProps) => (maps) => {
  return {
    ...{
      fullscreenControl: false,
      // panControl: false,
      // mapTypeControl: false,
      // scrollwheel: false,
      // styles: [{ stylers: [{ 'saturation': -100 }, { 'gamma': 0.8 }, { 'lightness': 4 }, { 'visibility': 'on' }] }]
    },
    ...extraProps,
  };
};

class EventsMap extends Component {
  el = null;

  static defaultProps = {
    defcenter: {
      lat: 40,
      lng: -80,
    },
    defzoom: 11,
  };
  state = { apiReady: false };

  mapLoaded = () => {
    this.setState({ apiReady: true });
  };
  /**
   * Given a list of events, calculate the northwest and southeast points of a
   * containing box.
   *
   * @param {Array<Object>} - events
   * @return {Object}
   */
  getNewBounds = (events) => {
    let nw = { lat: null, lng: null };
    let se = { lat: null, lng: null };

    const { isFiltered, selectedEvent, filters = {} } = this.props;

    const slicedEvents =
      (isFiltered || selectedEvent) && !filters.fromOrigin
        ? events.slice(0, filters.byCategory ? 100 : 12)
        : filters.fromOrigin
        ? events
            .sort((a, b) => a.distanceFromSearch - b.distanceFromSearch)
            .slice(0, 9)
        : events;
    //const slicedEvents = (filters.fromOrigin || selectedEvent) ? events.slice(0, 12) : events;

    console.log(`getting new bounds on ${slicedEvents.length} events`, {
      ...filters,
      selectedEvent,
    });

    slicedEvents
      .filter((x) => x.latitude && x.longitude)
      .forEach((event) => {
        const { latitude: rawlat, longitude: rawlng } = event;

        const latitude = parseFloat(rawlat);
        const longitude = parseFloat(rawlng);

        nw.lat =
          null === nw.lat || latitude > nw.lat ? latitude : parseFloat(nw.lat);
        nw.lng =
          null === nw.lng || longitude < nw.lng
            ? longitude
            : parseFloat(nw.lng);
        se.lat =
          null === se.lat || latitude < se.lat ? latitude : parseFloat(se.lat);
        se.lng =
          null === se.lng || longitude > se.lng
            ? longitude
            : parseFloat(se.lng);
      });

    if (nw.lat !== null && nw.lat === se.lat) {
      //console.log('single event', { latitude: nw.lat, longitude: se.lng })
      return { center: { lat: nw.lat, lng: se.lng }, zoom: 11 };
    }

    if (events.length > 1 && null !== nw.lat && null !== this.el) {
      const width = this.el.clientWidth - 15;
      const height = this.el.clientHeight - 15;
      //console.log('real bounds', { nw, se }, { width, height })
      return fitBounds({ nw, se }, { width, height });
    } else {
      //console.log('default', { center: { lat: 40, lng: -80 }, zoom: 5 })
      return { center: { lat: 40, lng: -80 }, zoom: 11 };
    }
  };

  /**
   * Given a list of events, returns marker components for each event at their
   * specified coordinates.
   *
   * @param {Array<Object>} - events
   * @return {Array<MapMarker>}
   */
  renderMarkers = (events) => {
    const { base_path, selectedEvent, setEvent, filters } = this.props;
    const selectedEventId = selectedEvent
      ? selectedEvent.post_id || selectedEvent.id
      : null;
    const latlngmap = {};

    //console.log(`rendering ${events.filter(x=>x.latitude && x.longitude).length} map markers from ${events.length} events`)
    // console.log('sc',
    //   supercluster(events, {
    //       minZoom: min_zoom, // min zoom to generate clusters on
    //       maxZoom, // max zoom level to cluster the points on
    //       radius: clusterRadius, // cluster radius in pixels
    //   })
    // );

    return events.map((event) => {
      const {
        id,
        post_id,
        is_wp_item = false,
        title,
        is_poi,
        latitude,
        longitude,
      } = event;
      const ll_key = "ll" + latitude + longitude;
      const noshadow = Boolean(latlngmap[ll_key]);
      latlngmap[ll_key] = true;
      const markerProps = {
        base_path,
        is_wp_item,
        id: post_id || id,
        post_id,
        is_poi,
        selectedEventId,
        title,
        latitude,
        longitude,
        noshadow,
        lat: latitude,
        lng: longitude,
        Icon: is_poi ? Star : undefined,
      };
      return (
        <MapMarker setThisEvent={() => setEvent(event)} {...markerProps} />
      );
    });
  };

  render(props) {
    const {
      apiKey,
      events,
      event,
      defcenter,
      defzoom,
      setEvent,
      selectedEvent,
      mapStyles,
      isLoading,
      extra_map_features = [],
      default_bounds,
      isFiltered,
      filters,
      default_zoom,
      min_zoom,
    } = props;

    const isDefault = !isFiltered && !selectedEvent;
    const isVirtualEvent = selectedEvent && !selectedEvent.latitude;

    const legitEvents = events.filter(
      (event) =>
        event.latitude &&
        event.longitude &&
        (!filters.fromOrigin ||
          !event.distanceFromSearch ||
          event.distanceFromSearch < 320)
    );

    const boundsEvents = selectedEvent
      ? [selectedEvent]
      : extra_map_features && extra_map_features.length
      ? legitEvents.concat(extra_map_features)
      : legitEvents;

    const mapEvents =
      extra_map_features && extra_map_features.length
        ? legitEvents.concat(extra_map_features)
        : legitEvents;

    const width = (this.el && this.el.clientWidth - 15) || 705;
    const height = (this.el && this.el.clientHeight - 15) || 584;

    const {
      center = { lat: 41.52269490543275, lng: -81.51461555554198 },
      zoom = 4,
    } =
      (isDefault || isVirtualEvent) && default_bounds
        ? fitBounds(default_bounds, { width, height })
        : boundsEvents.length
        ? this.getNewBounds(boundsEvents)
        : {};

    const _mapOptions = {
      styles: mapStyles,
      minZoom: min_zoom,
      zoom: zoom,
    };

    const mapOptions = createMapOptions(_mapOptions);

    console.log({
      mapOptions: _mapOptions,
      center,
      zoom,
      min_zoom,
      fitBounds: fitBounds(default_bounds, { width, height }),
    });

    return (
      <div
        class="wem__events-map"
        ref={(el) => (this.el = el)}
        data-events={legitEvents.length}
        data-poi={extra_map_features.length}
      >
        {isLoading ? (
          <Loader />
        ) : (
          <GoogleMapReact
            bootstrapURLKeys={{ key: apiKey }}
            center={center}
            zoom={zoom}
            defaultCenter={defcenter}
            defaultZoom={defzoom}
            minZoom={min_zoom}
            onGoogleApiLoaded={this.mapLoaded}
            onChange={(e) => console.log(e)}
            resetBoundsOnResize={true}
            //layerTypes={['TransitLayer']}
            yesIWantToUseGoogleMapApiInternals={true}
            options={mapOptions}
          >
            {!isVirtualEvent && this.renderMarkers(mapEvents)}
          </GoogleMapReact>
        )}
      </div>
    );
  }
}

export default EventsMap;
