import { h, Component } from "preact";
import { route } from "preact-router";

const threshold = 500;

class CategoryFilter extends Component {
  onChange(e) {
    e.preventDefault();
    const { events, setEvents } = this.props;
  }
  render(props, state) {
    const {
      categories,
      selectedCategory,
      style,
      setEvents,
      events,
      setFilter,
    } = props;
    if (!categories || !categories.length) {
      return null;
    }
    return (
      <div class="wem__filter wem__filter--category-section" style={style}>
        <label htmlFor="wem-em-category">Event Type</label>
        <div class="wem__filter--category-field">
          <select
            id="wem-em-category"
            onChange={(e) => setFilter({ byCategory: e.target.value || false })}
          >
            <option value="">(All)</option>
            {categories.map(({ slug, name }) => (
              <option selected={selectedCategory === slug} value={slug}>
                {name}
              </option>
            ))}
          </select>
        </div>
      </div>
    );
  }
}

const today = new Date().toISOString().substring(0, 10);

class DateFilter extends Component {
  render(props, state) {
    const { style, setFilter, selectedDate } = props;
    return (
      <div class="wem__filter wem__filter--date-section" style={style}>
        <label htmlFor="wem-em-date">Date</label>
        <div class="wem__filter--date-field">
          <input
            id="wem-em-date"
            type="date"
            onChange={(e) => setFilter({ fromDay: e.target.value || false })}
            min={today}
          />
        </div>
      </div>
    );
  }
}

const vfStyle = {
  width: "auto",
  marginTop: "10px",
  position: "relative",
};

// class IncludeVirtualFilter extends Component {
//   render(props, state) {
//     const { style, setFilter, virtualOnly = false } = props;
//     return (
//       <label
//         htmlFor="wem-virtual"
//         class={`virtual-toggle ${virtualOnly ? "checked" : "unchecked"}`}
//         style={vfStyle}
//       >
//         <span
//           class="virtual-label__large"
//           style={{ textTransform: "none", paddingRight: "10px" }}
//         >
//           Include Virtual Events?
//         </span>
//         <span class="virtual-label__small">Virtual?</span>
//         <input
//           title="Include Virtual Events?"
//           id="wem-virtual"
//           checked={virtualOnly}
//           type="checkbox"
//           onChange={(e) => setFilter({ virtualOnly: !virtualOnly })}
//         />
//       </label>
//     );
//   }
// }

export { CategoryFilter, DateFilter };
