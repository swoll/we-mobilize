import { h, Component } from 'preact';
import haversine from 'haversine';
import { route } from 'preact-router';
import classnames from '../../helpers/classnames';
import { gup } from '../../helpers';

const threshold = 5000;//120;

const initialZip = gup('zip').replace(/\D/g,'');

function isValidUSZip(sZip) {
   return /^\d{5}(-\d{4})?$/.test(sZip);
}


class SearchBar extends Component {
  state = {
    inputValue: '',
    error: '',
    currentSearch:''
  };

  onInput = (e) => {
    const { setFilter, isMobileSearch } = this.props;
    const { inputValue: oldInputState = '' } = this.state;
    const inputValue = e.target.value;
    this.setState({
      inputValue,
      error: '',
      currentSearch: inputValue ? this.state.currentSearch : ''
    });
    if(inputValue===""){
      this.props.toggleFilter(false);
      setFilter({ fromOrigin: false});
    }
    else if(inputValue && oldInputState.length!=5 && inputValue.length===5 && isValidUSZip(inputValue)){
      setTimeout(()=>{
        this.props.toggleFilter(true);
        this.onSubmit();
      }, 200);
    }
  };

  // componentDidMount() {
  //   if(gup('zip')){
  //     this.setState({
  //       inputValue: initialZip,
  //       error: ''
  //     });
  //     this.onSubmit();
  //     this.props.toggleFilter(true);
  //   }
  // }

  componentWillReceiveProps(nextProps, nextState) {
    const { started, toggleFilter } = this.props;
    const { inputValue } = this.state;
    //console.log('sb',{started, nextStarted: nextProps.started})
    if(!started && nextProps.started && initialZip){
      //console.log('initial zip', initialZip);
      this.setState({
        currentSearch: initialZip,
        inputValue:  initialZip
      });
      setTimeout(()=>{
        this.props.toggleFilter(true);
        this.onSubmit();
      }, 1600);
    }
    //console.log(nextState.inputValue, {nextState});
  }

  onSubmit = (e) => {
    const { toggleFilter, setFilter } = this.props;

    if(e){
      e.preventDefault();
    }
    //needs to move to the filters
    if (window.google) {
      const geocoder = new google.maps.Geocoder();
      const { events, setEvents, setEvent, base_path } = this.props;
      const { inputValue } = this.state;

      if (inputValue.length > 0) {
        geocoder.geocode({ address: `ZipCode ${inputValue}, USA`, region: 'US' }, (results, status) => {
          console.log(`zip search: `,{results})
          if (status === 'OK' && results.length > 0) {

            const { lat, lng } = results[0].geometry.location;
            const origin = { latitude: lat(), longitude: lng() };
            console.log('new origin',{origin})

            const filteredEvents = events.reduce((acc, event)=>{
              const {longitude, latitude} = event;
              const location = (latitude && longitude && ({longitude, latitude})) || (event && event.location && event.location.location);
              if(!location){
                return acc;
              }
              const distanceFromSearch = haversine(
                origin,
                location,
                {
                  unit: 'mile'
                }
              );
              event.distanceFromSearch = distanceFromSearch;
              if(distanceFromSearch < threshold){
                acc.push(event)
              }
              return acc;
            }, []).sort( (a,b) => a.distanceFromSearch - b.distanceFromSearch )

            route( "" );
            setEvent();
            toggleFilter(true);
            setFilter({ fromOrigin: inputValue});
            setEvents(filteredEvents);
            this.setState({
              currentSearch: inputValue
            })
          } else {
            if(status){
              this.setState({ error: 'No results found' });
            }
            console.error(`Geocoding failed w/ ${status}`);
          }
        });
      } else {
        setEvents(events);
        toggleFilter(false);
      }
    }
  };

  // componentWillReceiveProps(nextProps, {inputValue=""}){
  //   console.log(inputValue, this.state.inputValue);
  //   if(this.state.inputValue!==inputValue && !inputValue){
  //     this.props.toggleFilter(false);
  //   }
  // }

  render(props, state) {
    const { error, inputValue, currentSearch } = state;
    const { style, classNames='', started } = this.props;

    const searchClass = classnames("wem__search-bar", classNames);
    const buttonClass = classnames("wem__search-bar__icon fa fas fa-search", {'--active':inputValue && inputValue!==currentSearch})

    return (
      <form class={searchClass} onSubmit={this.onSubmit} style={style}>
        <button type="submit" class={buttonClass}></button>
        <input
          class="wem__search-bar__input"
          type="search"
          placeholder="Search by zip code..."
          value={inputValue}
          onInput={this.onInput}
          readonly={!started}
        />
        { error && <span class="wem__search-bar--error">{error}</span> }
      </form>
    );
  }
}

export default SearchBar;
