import { h, Component } from "preact";
import EventCard from "../EventCard";
import Loader from "../Loader";
import {
  timeString,
  eventMeta,
  cleanEntities,
  connectAddThis,
} from "../../helpers";

import SearchBar from "../SearchBar";
import {
  CategoryFilter,
  DateFilter,
  // IncludeVirtualFilter,
} from "../EventFilters";
import classnames from "../../helpers/classnames";
import { Caret } from "../Icons/Icons";

//import VirtualList from 'preact-virtual-list';
import VirtualList from "../VirtualList";

const tall_card_height = 340;
const short_card_height = 290;

function throttle(fn, threshhold = 250, scope) {
  var last, deferTimer;
  return function () {
    var context = scope || this;

    var now = +new Date(),
      args = arguments;
    if (last && now < last + threshhold) {
      // hold on to it
      clearTimeout(deferTimer);
      deferTimer = setTimeout(function () {
        last = now;
        fn.apply(context, args);
      }, threshhold);
    } else {
      last = now;
      fn.apply(context, args);
    }
  };
}
const window_width = window.outerWidth;

class EventsPanel extends Component {
  constructor() {
    super();
    this.runOnScroll = throttle(this.runOnScroll, 50, this);
    this.runOnVirtualScroll = throttle(this.runOnVirtualScroll, 50, this);
    this.virtualResize = throttle(this.virtualResize, 50, this);
    this.virtualResizeSlow = throttle(this.virtualResize, 300, this);
  }
  state = {
    nearSticky: false,
    sticky: false,
    bottom: false,
    hasScrolled: false,
    topPos: 0,
    fullPanel: false,
    virtualHeight: "60%",
    itemHeight:
      window_width < 1300 && window_width > 1000
        ? tall_card_height
        : short_card_height,
  };
  runOnScroll(e) {
    const { sticky, nearSticky, bottom, hasScrolled } = this.state;
    const { offsetTop: spacerTop } = this._spacer;
    const { scrollTop, scrollHeight } = this._scroller;
    //console.log({hasScrolled, spacerTop, home: this._spacerHome, scrollTop, scrollHeight, offsetHeight: this._offsetHeight})
    if (!hasScrolled) {
      this.setState({ hasScrolled: true });
    }
    if (!sticky && this._spacerHome < spacerTop) {
      this.setState({ sticky: true });
    } else if (
      sticky &&
      (this._spacerHome === spacerTop || scrollTop < this._spacerHome)
    ) {
      this.setState({ sticky: false });
    }

    if (!nearSticky && this._spacerHome < scrollTop + 105) {
      this.setState({ nearSticky: true });
    } else if (nearSticky && this._spacerHome > scrollTop + 105) {
      this.setState({ nearSticky: false });
    }

    if (!bottom && scrollTop / scrollHeight > 0.9) {
      //scrollHeight < scrollTop + this._offsetHeight + 20){
      this.setState({ bottom: true });
    } else if (bottom && scrollTop / scrollHeight < 0.9) {
      //(scrollHeight > scrollTop + this._offsetHeight)){
      this.setState({ bottom: false });
    }
  }
  runOnVirtualScroll({ height, offset }) {
    if (!this._scroller) {
      return false;
    }
    const { sticky, nearSticky, bottom, hasScrolled } = this.state;
    //const { offsetTop: spacerTop } = this._spacer;
    const { scrollTop, scrollHeight } = this._scroller;
    if (!hasScrolled && offset > 0) {
      this.setState({ hasScrolled: true });
    }
    if (!sticky && offset > scrollHeight) {
      this.setState({
        sticky: true,
        fullPanel: true,
      });
    } else if (sticky && offset < scrollHeight) {
      this.setState({
        sticky: false,
        fullPanel: false,
      });
    }

    if (!bottom && offset / height > 0.9) {
      //scrollHeight < scrollTop + this._offsetHeight + 20){
      this.setState({ bottom: true });
    } else if (bottom && offset / height < 0.9) {
      //(scrollHeight > scrollTop + this._offsetHeight)){
      this.setState({ bottom: false });
    }
    //connectAddThis();
    //this.virtualResizeSlow();
  }
  virtualResize() {
    const { sticky, nearSticky, bottom, hasScrolled } = this.state;
    const { outerWidth } = window;
    this.setState({
      virtualHeight:
        this._outerPanel.offsetHeight - this._scroller.offsetHeight,
      itemHeight:
        outerWidth < 1300 && outerWidth > 1000
          ? tall_card_height
          : short_card_height,
    });
    setTimeout(() => {
      this.setState({
        virtualHeight:
          this._outerPanel.offsetHeight - this._scroller.offsetHeight,
        itemHeight:
          outerWidth < 1300 && outerWidth > 1000
            ? tall_card_height
            : short_card_height,
      });
    }, 10);
    //console.log({oh: this._outerPanel.offsetHeight, sch: this._scroller.offsetHeight})
  }
  componentDidMount() {
    const { isVirtual, events } = this.props;
    const { offsetTop } = this._spacer;
    const { scrollHeight, offsetHeight } = this._scroller;
    this._spacerHome = offsetTop;
    this._scrollHeight = scrollHeight;
    this._offsetHeight = offsetHeight;
    if (!isVirtual) {
      this._scroller.addEventListener("scroll", this.runOnScroll);
      connectAddThis();
    } else {
      this.timeout = setTimeout(this.virtualResize, 100);
      window.addEventListener("resize", this.virtualResize);
    }
    //console.log(`${events.length} events listed`,{isVirtual, spacerTop: offsetTop, home: this._spacerHome, div: this._scroller.offsetTop})
  }
  componentWillUnmount() {
    const { isVirtual } = this.props;
    clearTimeout(this.timeout);
    if (!isVirtual) {
      this._scroller.removeEventListener("scroll", this.runOnScroll);
    } else {
      window.removeEventListener("resize", this.virtualResize);
    }
  }
  render(props, state) {
    const {
      base_path,
      title,
      description,
      events,
      setEvent,
      setEvents,
      categories = [],
      isVirtual,
      toggleFilter,
      virtualOnly = false,
      setFilter,
      isFiltered,
      isLoading,
      no_events_message,
      show_images = true,
      link_source,
    } = props;
    const {
      sticky,
      nearSticky,
      bottom,
      hasScrolled,
      topPos,
      fullPanel,
      virtualHeight,
      itemHeight,
    } = state;

    const panelClass = classnames("wem__events-right wem__events-panel", {
      "--at-bottom": bottom,
      "wem__events-panel-virtual": isVirtual,
    });
    const scrollableClass = classnames("wem__events-panel__scrollable", {
      "--sticky is-sticky": sticky,
      "--near-sticky": nearSticky,
      "--no-events": !isLoading && events.length === 0,
      "--no-description": !Boolean(description),
    });

    console.log(
      events.length,
      events.filter((x) => x.is_virtual).length,
      events.filter((x) => !x.is_virtual).length
    );

    const with_images =
      window.outerWidth > 767 && window.outerWidth < 1000 ? false : show_images;

    const virtualClass = classnames("wem__events-virtual-scroller", {
      "--is-full": fullPanel,
    });

    return (
      <div
        class={panelClass}
        data-events={events.length}
        ref={(outerPanel) => (this._outerPanel = outerPanel)}
      >
        <div
          class={scrollableClass}
          ref={(scroller) => (this._scroller = scroller)}
          style={{ top: topPos }}
        >
          <div class="wem__events-panel__content">
            <h1 class="wem__events-panel__headline h3">{title}</h1>
            {description && (
              <div class="wem__events-panel__description">
                {cleanEntities(description)}
              </div>
            )}

            <SearchBar
              classNames="only-mobile"
              started={!isLoading}
              isMobileSearch={true}
              setFilter={setFilter}
              base_path={base_path}
              events={events}
              setEvents={setEvents}
              setEvent={setEvent}
              toggleFilter={toggleFilter}
            />
          </div>
          <div class="wem_events-filters">
            <CategoryFilter categories={categories} setFilter={setFilter} />
            <DateFilter setFilter={setFilter} />
          </div>
          <div
            class="wem__events-panel__scroll-spacer"
            ref={(spacer) => (this._spacer = spacer)}
          ></div>
          {!isLoading &&
            events.length > 0 &&
            !isVirtual &&
            events.map((event, i) => (
              <EventCard
                link_source={link_source}
                show_images={show_images && (hasScrolled || i < 4)}
                base_path={base_path}
                event={event}
                setEvent={setEvent}
              />
            ))}
        </div>
        {isLoading && <Loader style="margin-top: 150px;" />}
        {!isLoading && isVirtual && events.length > 0 && (
          <VirtualList
            class={virtualClass}
            data={events}
            style={{ height: virtualHeight }}
            updatePos={this.runOnVirtualScroll}
            rowHeight={itemHeight}
            renderRow={(event) => (
              <EventCard
                link_source={link_source}
                isVirtual={isVirtual}
                itemHeight={itemHeight}
                show_images={with_images}
                base_path={base_path}
                event={event}
                setEvent={setEvent}
              />
            )}
          />
        )}
        {!isLoading &&
          events.length === 0 &&
          (isFiltered ? (
            <div class="wem__events-panel__noevents --isFiltered">
              {no_events_message}{" "}
              <button onClick={() => setFilter({ clear: true })}>
                Clear filters?
              </button>
            </div>
          ) : (
            <div class="wem__events-panel__noevents">{no_events_message}</div>
          ))}
        {!isLoading && events.length > 0 && (
          <div class="wem__events-panel__scrollmore">
            <Caret strokewidth="5" />
          </div>
        )}
      </div>
    );
  }
}

export default EventsPanel;
