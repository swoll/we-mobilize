<?php
    $mobilize = acf_add_options_sub_page(array(
      'page_title'  => 'Mobilize Events Settings',
      'menu_title'  => 'Settings',
      'menu_slug' => 'mobilize',
      'capability' => 'edit_posts',
      "post_id"    => "mobilize_event",
      "parent"     => "edit.php?post_type=mobilize_event",
    ));

add_action( 'wp_loaded', array ( Mobilize_Sync::get_instance(), 'register' ) );
//mobilize_event_page_mobilize

class Mobilize_Orgs {

    public function get_orgs(){
      //delete_transient( 'we-mobilize-orgs');
      $orgs = get_transient( 'we-mobilize-orgs');

      if(!$orgs){
        $response = wp_remote_get( "https://events.mobilizeamerica.io/api/v1/organizations?per_page=500" );

        if( !is_wp_error( $response ) ) {
          $body = wp_remote_retrieve_body($response);
          $orgs = json_decode($body, true);

          if($orgs && array_key_exists("data", $orgs)){
            set_transient( 'we-mobilize-orgs', $orgs['data'], 3600);
          }
          else{
            return false;
          }
        }
        else{
          return false;
        }
      }
      if($orgs && array_key_exists("data", $orgs)){
        set_transient( 'we-mobilize-orgs', $orgs['data'], 3600);
        return $orgs['data'];
      }else{
        return false;
      }
    }

    public function mobilize_get_org($org_id = false) {
        if(!$org_id){
          $org_id = get_field('mobilize_organization_id', 'mobilize_event');
        }
        if(!$org_id){
          return false;
        }
        $orgs = $this->get_orgs();
        if($orgs){
          $org_info = array_filter(
              $orgs,
              function ($e) use (&$org_id) {
                return $e['id'] == $org_id;
              }
          );
          return $org_info;
        }
        else{
          return false;
        }
    }
}

//mobilize_event_page_mobilize



// $orgs = new Mobilize_Orgs();
// var_dump($orgs->mobilize_get_org(203));
// exit();

class Mobilize_Sync{
    /**
     * Plugin instance.
     *
     * @see get_instance()
     * @type object
     */
    protected static $instance = NULL;

    protected $sync_action     = 'mobilize_sync';
    protected $sync_option_name     = 'mobilize_sync';
    protected $purge_action     = 'mobilize_purge';
    protected $purge_option_name     = 'mobilize_purge';
    protected $dedupe_action       = 'mobilize_dedupe_events';
    protected $dedupe_option_name  = 'mobilize_dedupe_events';
    protected $page_id = NULL;

    /**
     * Access this plugin’s working instance
     *
     * @wp-hook wp_loaded
     * @return  object of this class
     */
    public static function get_instance()
    {
        NULL === self::$instance and self::$instance = new self;
        return self::$instance;
    }

    public function get_orgs(){

      $orgs = get_transient( 'we-mobilize-orgs');

      if(!$orgs){
        $response = wp_remote_get( "https://events.mobilizeamerica.io/api/v1/organizations?per_page=500" );

        if( !is_wp_error( $response ) ) {
          $body = wp_remote_retrieve_body($response);
          $orgs = json_decode($body, true);

          if($orgs && array_key_exists("data", $orgs)){
            set_transient( 'we-mobilize-orgs', $orgs['data'], 3600);
            return $orgs['data'];
          }
          else{
            return false;
          }
        }
        else{
          return false;
        }
      }
      else{
        return $orgs;
      }
    }

    public function mobilize_get_org($org_id = false) {
        if(!$org_id){
          $org_id = get_field('mobilize_organization_id', 'mobilize_event');
        }
        if(!$org_id){
          return false;
        }
        $orgs = $this->get_orgs();

        if($orgs){
          $org_info = NULL;
          foreach ( $orgs as $org ) {
              if ( $org_id == $org['id'] ) {
                  $org_info = $org;
              }
          }
          return $org_info;
        }
        else{
          return false;
        }
    }

    public function register()
    {
        add_action( 'admin_menu', array ( $this, 'add_menu' ) );
        add_action( "admin_post_$this->sync_action", array ( $this, 'mob_admin_update' ) );
        add_action( "admin_post_$this->purge_action", array ( $this, 'mob_admin_update' ) );
        add_action( "admin_post_$this->dedupe_action", array ( $this, 'mob_admin_update' ) );
    }

    public function add_menu()
    {
        $page_id = add_options_page(
            'Mobilize Sync',
            'Mobilize Sync',
            'manage_options',
            'mobilize-sync',
            array ( $this, 'render_options_page' )
        );

        add_action( "load-$page_id", array ( $this, 'parse_message' ) );
    }

    public function parse_message()
    {
        if ( ! isset ( $_GET['msg'] ) )
            return;

        $text = FALSE;

        $this->msg_text = $_GET['msg'];

        if ( $this->msg_text ){
            add_action( 'admin_notices', array ( $this, 'render_msg' ) );
        }
    }

    public function render_msg(){
        echo '<br><div class="notice notice-success is-dismissible '.(strpos($_GET['msg'], 'deleted')?'deleted':'updated').'"><p>'. $this->msg_text . '</p></div>';
    }

    public function timeUntil($time) {

        $since = $time - time();

        $string     = '';

        $chunks = array(
            array(60 * 60 * 24 , 'day'),
            array(60 * 60 , 'hour'),
            array(60 , 'minute'),
            array(1 , 'second')
        );

        for ($i = 0, $j = count($chunks); $i < $j; $i++) {
            $seconds = $chunks[$i][0];
            $name = $chunks[$i][1];
            if (($count = floor($since / $seconds)) != 0) {
                break;
            }
        }

        $string = ($count == 1) ? '1 ' . $name . ' ago' : $count . ' ' . $name . 's until';

        return $string;

    }

    public function render_options_page()
    {
        $redirect = urlencode(remove_query_arg( 'msg', $_SERVER['REQUEST_URI'] ));
        $sync_setting = get_field('mobilize_sync_setting', 'mobilize_event');
        $sync_text_array = Array(
          'hourly' => 'Hourly',
          'twicedaily' => 'Twice Daily',
          'daily' => 'Daily');

        $num_posts = wp_count_posts( 'mobilize_event' )->publish;
        $can_sync = false;
        echo "<style>.yoast-alert.notice.notice-warning.is-dismissible{display:none;}</style>"
        ?><h1><?php echo $GLOBALS['title']; ?></h1>
        <h2><?php
          $org_id = get_field('mobilize_organization_id', 'mobilize_event');

          if($org_id){
            $org_info = $this->mobilize_get_org($org_id);
            if($org_info && array_key_exists("name", $org_info)){
              if ($sync_setting != 'off'){
                $sync_text = $sync_text_array[$sync_setting];
                echo "<h2>Currently Syncing {$sync_text} w/: ".$org_info['name'].'</h2>';
                $cron_job_ts = wp_next_scheduled('mobilize_cron_hook');
                if($cron_job_ts > 0 && $cron_job_ts > time() ){
                  echo "<p>"; echo $this->timeUntil($cron_job_ts); echo " next scheduled sync.</p>";
                }
              }else {
                echo '<h2>Manual Sync w/: '.$org_info['name'].'</h2>';
              }
              $can_sync = true;
            }else{
              echo '<h3>You must <a href="/wp-admin/edit.php?post_type=mobilize_event&page=mobilize">choose a valid organization</a> to sync by its Id.</h3>';
            }
          }
          else{
            echo '<h3>You must <a href="/wp-admin/edit.php?post_type=mobilize_event&page=mobilize">choose a valid organization</a> to sync by its Id.</h3>';
          }
        ?>
        <p><?php echo $num_posts; ?> events synced</p>
          <?php if ($can_sync) : ?>
          <form action="<?php echo admin_url( 'admin-post.php' ); ?>" method="POST">
              <input type="hidden" name="action" value="<?php echo $this->sync_action; ?>">
              <?php wp_nonce_field( $this->sync_action, $this->sync_option_name . '_nonce', FALSE ); ?>
              <input type="hidden" name="_wp_http_referer" value="<?php echo $redirect; ?>">
              <input type="hidden" name="<?php echo $this->sync_option_name; ?>" id="<?php echo $this->sync_option_name; ?>" value="true">
              <?php submit_button( $num_posts>0 ? 'Resync Events':'Sync Events' ); ?>
          </form>
        <?php
          endif;
          if($num_posts>0){
          ?>
          <form id="purge-all-events" action="<?php echo admin_url( 'admin-post.php' ); ?>" method="POST">
              <input type="hidden" name="action" value="<?php echo $this->purge_action; ?>">
              <?php wp_nonce_field( $this->purge_action, $this->purge_option_name . '_nonce', FALSE ); ?>
              <input type="hidden" name="_wp_http_referer" value="<?php echo $redirect; ?>">

              <input type="hidden" name="<?php echo $this->purge_option_name; ?>" id="<?php echo $this->purge_option_name;?>" value="true">
              <?php submit_button( 'Purge All Events' , 'delete'); ?>
          </form>
          <script>document.querySelector('#purge-all-events').addEventListener('submit',event => confirm( 'Are you sure you want to delete all Mobilize events?' ) || event.preventDefault() && false) </script>
          <form action="<?php echo admin_url( 'admin-post.php' ); ?>" method="POST">
              <input type="hidden" name="action" value="<?php echo $this->dedupe_action; ?>">
              <?php wp_nonce_field( $this->dedupe_action, $this->dedupe_option_name . '_nonce', FALSE ); ?>
              <input type="hidden" name="_wp_http_referer" value="<?php echo $redirect; ?>">

              <input type="hidden" name="<?php echo $this->dedupe_option_name; ?>" id="<?php echo $this->dedupe_option_name;?>" value="true">
              <?php submit_button( 'Debug Events' , 'delete'); ?>
          </form>
          <p>Short form events list is available at the <a href="/wp-json/we-mobilize/v2/short" target="_blank"><code>/wp-json/we-mobilize/v2/short</code></a> endpoint.</p>
          <p>All events with all details are available at the <a href="/wp-json/we-mobilize/v2/long" target="_blank"><code>/wp-json/we-mobilize/v2/long</code></a> endpoint.</p>
          <p>Individual events are available at the <code>/wp-json/we-mobilize/v2/event?post_id={post_id}</code> endpoint.</p>
          <p>Original Mobilize Endpoint available at: <a target="_blank" href="<?php echo htmlspecialchars(get_option('we_mobilize_endpoint')); ?>"><code><?php echo htmlspecialchars(get_option('we_mobilize_endpoint')); ?></code></a></p>
        <?php
        }
    }

    public function mob_admin_update(){

        set_time_limit(3600);

        if ( !wp_verify_nonce( $_POST[ $this->sync_option_name . '_nonce' ], $this->sync_action ) 
          && !wp_verify_nonce( $_POST[ $this->purge_option_name . '_nonce' ], $this->purge_action )
          && !wp_verify_nonce( $_POST[ $this->dedupe_option_name . '_nonce' ], $this->dedupe_action )
        ){
            die( 'Invalid nonce.' . var_export( $_POST, true ) );
        }
        if ( !isset ( $_POST['_wp_http_referer'] ) ){
            die( 'Missing redirect.' );
        }

        if ( isset ( $_POST[ $this->sync_option_name ] ) ){
          $sync_setting = get_field('mobilize_sync_setting', 'mobilize_event');
          if (isset($sync_setting) && 'off' != $sync_setting){
            wp_clear_scheduled_hook( 'mobilize_cron_hook' );
            wp_schedule_event( time(), $sync_setting, 'mobilize_cron_hook' );
            add_action( 'mobilize_cron_hook', 'mobilize_get_events_exec' );
          }
          $msg = mobilize_get_events_exec();
        }
        else if( isset ( $_POST[ $this->purge_option_name ] ) ){
          $msg = mobilize_purge_events();
        }
        else if( isset ( $_POST[ $this->dedupe_option_name ] ) ){
          $msg = we_mobilize_dedupe_events();
        }
        else {
          $msg = "no action";
        }

        $url = add_query_arg( 'msg', urlencode($msg), urldecode( $_POST['_wp_http_referer'] ) );

        wp_safe_redirect( $url );
        exit;
    }
}