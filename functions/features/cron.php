<?php

//https://events.mobilizeamerica.io/api/v1/events?organization_id=203

//"venue":"Alexandria Democratic Committee Office","address_lines":["618 N Washington St",""],"locality":"Alexandria","region":"VA","postal_code":"22314"

function create_sync_post( $event, $id_for_update = null ) {

	set_time_limit( 360 );

	$start_datestamp = new DateTime();
	$end_datestamp   = new DateTime();

	$start_datestamp->setTimezone( new DateTimeZone( $event['timezone'] ) );
	$end_datestamp->setTimezone( new DateTimeZone( $event['timezone'] ) );

	$start_datestamp->setTimestamp( $event['timeslots'][0]['start_date'] );
	$end_datestamp->setTimestamp( end( $event['timeslots'] )['end_date'] );

	$meta_input = array(
		'mobilize_id'        => $event['id'],
		'is_priority'        => $event['high_priority'],
		'mobilize_json'      => wp_slash( json_encode( $event ) ),
		'description'        => wp_slash( json_encode( $event['description'] ) ),
		'timezone'           => $event['timezone'],
		'featured_image_url' => $event['featured_image_url'],
		'timeslots'          => $event['timeslots'],
		'first_day'          => $event['timeslots'][0]['start_date'],
		'event_start_time'   => $start_datestamp->format( 'Y-m-d H:i:s' ),
		'event_end_time'     => $end_datestamp->format( 'Y-m-d H:i:s' ),
	);

	if ( array_key_exists( 'location', $event ) && $event['location'] ) {
		if ( array_key_exists( 'location', $event['location'] ) && array_key_exists( 'latitude', $event['location']['location'] ) ) {
			$meta_input['lat'] = $event['location']['location']['latitude'];
			$meta_input['lng'] = $event['location']['location']['longitude'];
		}
		if ( array_key_exists( 'region', $event['location'] ) && array_key_exists( 'locality', $event['location'] ) ) {
			$meta_input['citystate'] = $event['location']['locality'] . ' ' . $event['location']['region'];
		}
		if ( array_key_exists( 'postal_code', $event['location'] ) ) {
			$meta_input['zip'] = $event['location']['postal_code'];
		}
		if ( array_key_exists( 'address_lines', $event['location'] ) && is_array( $event['location']['address_lines'] ) ) {
			$meta_input['address'] = join( ', ', $event['location']['address_lines'] ) . ' ' . $event['location']['locality'] . ' ' . $event['location']['region'] . ', ' . $event['location']['postal_code'];
		}
		if ( array_key_exists( 'venue', $event['location'] ) ) {
			$meta_input['venue'] = $event['location']['venue'];
		}
	}

	$args = array(
		'post_name'      => sanitize_title( $event['title'] . '-' . $event['id'], $event['id'] ),
		'post_title'     => $event['title'],
		'post_date'      => date( 'Y-m-d H:i:s', $event['created_date'] ),
		//'post_modified' => current_time( 'mysql' ),//wp is dumb and won't let this work
		'post_content'   => $event['description'],
		'post_type'      => 'mobilize_event',
		'post_author'    => 1,
		'comment_status' => 'closed',
		'ping_status'    => 'closed',
		'post_status'    => 'publish',
		'meta_input'     => $meta_input,
		'tax_input'      => array( $event['event_type'] ),
	);

	if ( $event['event_type'] ) {
		$args['tax_input'] = array(
			'event_types' => array( $event['event_type'] ),
		);
	}

	if ( isset( $id_for_update ) ) {
		$args['ID'] = $id_for_update;
		//echo 'updating '.$id_for_update.' to '.current_time( 'mysql' );
		$post_id = wp_update_post( $args );
	} else {
		$post_id = wp_insert_post( $args );
		wp_update_post( array( 'ID' => $post_id ) );//bump to make post_modified = now
	}
	if ( $post_id ) {
		// $taxonomy = 'event_types';
		// $termObj  = get_term_by( 'slug', $event['event_type'], $taxonomy);
		// wp_set_object_terms($post_id, $termObj, $taxonomy);
		return $post_id;
	} else {
		return false;
	}
}

function mobilize_purge_events() {

	set_time_limit( 3600 );

	add_filter( 'wpseo_enable_notification_post_trash', '__return_false' );
	$all_events = get_posts(
		array(
			'numberposts' => -1,
			'post_type'   => 'mobilize_event',
		)
	);
	$num        = count( $all_events );

	if ( 0 < $num ) {
		foreach ( $all_events as $event ) {
			wp_delete_post( $event->ID, true );
		}
	}
	return "{$num} events deleted";
}

function we_mobilize_dedupe_events() {
	set_time_limit( 3600 );
	$synced_events       = get_posts(
		array(
			'numberposts' => -1,
			'post_type'   => 'mobilize_event',
		)
	);
	$starting_events_num = count( $synced_events );

	$synced_events = array_map(
		function( $event ) {
			return intval( get_post_meta( $event->ID, 'mobilize_id', true ) );
		},
		$synced_events
	);

	if ( ! $org_id ) {
		$org_id = get_field( 'mobilize_organization_id', 'mobilize_event' );
	}
	$today         = strtotime( 'today midnight' );//date_create( date('Y-m-d'), timezone_open( 'UTC' ) )->getTimestamp();
	$only_future   = 'future' === get_field( 'which_events_should_sync', 'mobilize_event' );
	$endpoint_type = get_field( 'mobilize_endpoint', 'mobilize_event' );
	$endpoint      = ( $endpoint_type === 'all_orgs' ) ?
	"https://events.mobilizeamerica.io/api/v1/organizations/{$org_id}/events?per_page=200" . ( $only_future ? "&amp;timeslot_start=gt_{$today}" : '' ) :
	"https://events.mobilizeamerica.io/api/v1/events?organization_id={$org_id}&per_page=200" . ( $only_future ? "&amp;timeslot_start=gt_{$today}" : '' );
	$response      = wp_remote_get( $endpoint );

	if ( ! is_wp_error( $response ) ) {
		$body            = wp_remote_retrieve_body( $response );
		$decodedResponse = json_decode( $body, true );
		//update_option('last-mobilize-response-json', $response);
		$eventdata = $decodedResponse['data'];

		while ( array_key_exists( 'next', $decodedResponse ) && null != $decodedResponse['next'] && 'null' != $decodedResponse['next'] ) {
			$response = wp_remote_get( $decodedResponse['next'] );

			if ( ! is_wp_error( $response ) ) {
				$body            = wp_remote_retrieve_body( $response );
				$decodedResponse = json_decode( $body, true );
				$eventdata       = array_merge( $eventdata, $decodedResponse['data'] );
			} else {
				break;
			}
		}

		$api_events_num = count( $eventdata );

		$eventdata = array_map(
			function( $event ) {
				return intval( $event['id'] );
			},
			$eventdata
		);

		$difference   = array_diff( $synced_events, $eventdata );
		$dupes        = array_diff_assoc( $synced_events, array_unique( $synced_events ) );
		$dupeids      = array();
		$dupeid_count = 0;
		if ( count( $dupes ) ) {
			foreach ( $dupes as $dupe ) {
				$args             = array(
					'post_type'      => 'mobilize_event',
					'posts_per_page' => 6,
					'meta_query'     => array(
						array(
							'key'     => 'mobilize_id',
							'value'   => intval( $dupe ),
							'compare' => '=',
						),
					),
				);
				$events           = get_posts( $args );
				$dupeids[ $dupe ] = array();
				foreach ( $events as $event ) {
					array_push( $dupeids[ $dupe ], $event->ID );
				}
			}
		}

		if ( ( $starting_events_num != $api_events_num ) || count( $difference ) ) {
			return "Odd events (Sync'd: {$starting_events_num} vs Api {$api_events_num}): " . print_r( $difference, true ) . '<br><br>Dupes: ' . print_r( $dupes, true ) . '<br><br>Dupe PostIds: ' . $dupeid_count . ' ' . print_r( $dupeids, true ) . '<pre>' . $endpoint . '</pre>';
		} else {
			return "Events match. {$starting_events_num} vs {$api_events_num}";
		}
	} else {
		return 'error';
	}
}

function mobilize_get_events_exec( $org_id = false ) {
	set_time_limit( 3600 );
	$synced_events   = get_posts(
		array(
			'numberposts' => -1,
			'post_type'   => 'mobilize_event',
		)
	);
	$starting_events = count( $synced_events );

	$synced_events_map = array();

	foreach ( $synced_events as $event ) {
		$mid = get_post_meta( $event->ID, 'mobilize_id', true );

		$synced_events_map[ $mid ] = array(
			'post_id'       => $event->ID,
			'post_modified' => $event->post_modified,
		);
	}

	if ( ! $org_id ) {
		$org_id = get_field( 'mobilize_organization_id', 'mobilize_event' );
	}
	$only_future    = 'future' === get_field( 'which_events_should_sync', 'mobilize_event' );
	$post_type      = 'mobilize_event';
	$added_events   = 0;
	$updated_events = 0;
	$skipped_events = 0;
	$removed_events = 0;
	$today          = strtotime( 'today midnight' );//date_create( date('Y-m-d'), timezone_open( 'UTC' ) )->getTimestamp();

	$endpoint_type = get_field( 'mobilize_endpoint', 'mobilize_event' );

	$endpoint = ( $endpoint_type === 'all_orgs' ) ?
	"https://events.mobilizeamerica.io/api/v1/organizations/{$org_id}/events?per_page=200" . ( $only_future ? "&timeslot_start=gt_{$today}" : '' ) :
	"https://events.mobilizeamerica.io/api/v1/events?organization_id={$org_id}&per_page=200" . ( $only_future ? "&timeslot_start=gt_{$today}" : '' );

	update_option( 'we_mobilize_endpoint', $endpoint, false );

	//if(post_type_exists($post_type)){
	$response = wp_remote_get( $endpoint );

	if ( ! is_wp_error( $response ) ) {
		$body            = wp_remote_retrieve_body( $response );
		$decodedResponse = json_decode( $body, true );
		//update_option('last-mobilize-response-json', $response);
		$eventdata = $decodedResponse['data'];

		while ( null != $decodedResponse['next'] ) {
			$response = wp_remote_get( $decodedResponse['next'] );

			if ( ! is_wp_error( $response ) ) {
				$body            = wp_remote_retrieve_body( $response );
				$decodedResponse = json_decode( $body, true );
				$eventdata       = array_merge( $eventdata, $decodedResponse['data'] );
			} else {
				break;
			}
		}

		$now = time();

		$got_events = count( $eventdata );

		foreach ( $eventdata as $event ) {

			if ( array_key_exists( $event['id'], $synced_events_map ) ) {
				$updated_events++;
				create_sync_post( $event, $synced_events_map[ $event['id'] ]['post_id'] );
				unset( $synced_events_map[ $event['id'] ] );
			} else {
				$added_events++;
				create_sync_post( $event );
			}
		}

		$remaining_events = count( $synced_events_map );

		//delete old events
		foreach ( $synced_events_map as $old_event ) {
			wp_delete_post( $old_event['post_id'], true );
			$removed_events++;
		}

		update_option( 'we_mobilize_json_full', json_encode( $eventdata ), false );
		//update_option('we_mobilize_json_short', $response);
		delete_transient( 'we-mobilize-long' );
		return "Started with {$starting_events} events, saw {$got_events} api events. Added {$added_events} events, updated {$updated_events} events, & removed {$removed_events} events";
	}
	//}
}

$sync_setting = get_field( 'mobilize_sync_setting', 'mobilize_event' );

if ( isset( $sync_setting ) && $sync_setting != 'off' && null !== get_field( 'mobilize_organization_id', 'mobilize_event' ) ) {
	if ( ! wp_next_scheduled( 'mobilize_cron_hook' ) ) {
		wp_schedule_event( time(), $sync_setting, 'mobilize_cron_hook' );
	}
	add_action( 'mobilize_cron_hook', 'mobilize_get_events_exec' );
}
// if(isset($sync_setting) && $sync_setting != 'off' && wp_next_scheduled( 'mobilize_cron_hook' )){
//   wp_unschedule_event( wp_next_scheduled( 'mobilize_cron_hook' ), 'mobilize_cron_hook' );
// }

function we_update_cron( $value, $post_id, $field ) {
	$sync_setting = get_field( 'mobilize_sync_setting', 'mobilize_event' );

	if ( isset( $sync_setting ) && $sync_setting != $value ) {
		wp_clear_scheduled_hook( 'mobilize_cron_hook' );
		wp_schedule_event( time(), $value, 'mobilize_cron_hook' );
	}
	add_action( 'mobilize_cron_hook', 'mobilize_get_events_exec' );

	return $value;
}

//when the settings change, reget all events
function update_org_id( $option_name, $old_value, $value = '' ) {
	global $wpdb;
	//$screen = get_current_screen();
	//echo $option_name . ' '. $old_value .' '. $value; exit();
	if ( ( $option_name == 'mobilize_event_mobilize_organization_id' || $option_name == 'mobilize_event_which_events_should_sync' ) && $value != '' ) {
		mobilize_purge_events();
		$wpdb->get_results( "DELETE FROM wp_terms WHERE term_id IN ( SELECT * FROM ( SELECT wp_terms.term_id FROM wp_terms JOIN wp_term_taxonomy ON wp_term_taxonomy.term_id = wp_terms.term_id WHERE taxonomy = 'event_types' AND count = 0 ) as T)" );
		if ( $value == 'all' || $value == 'future' ) {
			$value = get_field( 'mobilize_organization_id', 'mobilize_event' );
		}
		$response = mobilize_get_events_exec( $value );
		//update_option('last-mobilize-response', $response);
		// echo $response;
		// exit();
	}
	if ( $option_name == 'mobilize_event_mobilize_endpoint' && $value != '' && get_field( 'mobilize_organization_id', 'mobilize_event' ) ) {
		$org_id      = get_field( 'mobilize_organization_id', 'mobilize_event' );
		$only_future = get_field( 'which_events_should_sync', 'mobilize_event' );
		$today       = date_create( date( 'Y-m-d' ), timezone_open( 'UTC' ) )->getTimestamp();
		$endpoint    = ( $value === 'all_orgs' ) ?
		"https://events.mobilizeamerica.io/api/v1/organizations/{$org_id}/events?per_page=200" . ( $only_future ? "&timeslot_start=gt_{$today}" : '' ) :
		"https://events.mobilizeamerica.io/api/v1/events?organization_id={$org_id}&per_page=200" . ( $only_future ? "&timeslot_start=gt_{$today}" : '' );

		update_option( 'we_mobilize_endpoint', $endpoint, false );
	}
}
add_filter( 'acf/update_value/name=mobilize_sync_setting', 'we_update_cron', 10, 3 );
add_action( 'added_option', 'update_org_id', 10, 3 );
add_action( 'updated_option', 'update_org_id', 10, 3 );
add_action( 'updated_option_mobilize_events', 'update_org_id', 10, 2 );
