<?php
add_action( 'init', 'cpt_office' );
function cpt_office() {
    register_post_type( 'office',
        array(
            'labels' => array(
                'name' => 'Offices',
                'singular_name' => 'Office',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Office',
                'edit' => 'Edit',
                'edit_item' => 'Edit Office',
                'new_item' => 'New Office',
                'view' => 'View',
                'view_item' => 'View Office',
                'search_items' => 'Search Office',
                'not_found' => 'No Office found',
                'not_found_in_trash' => 'No Office found in Trash',
                'parent' => 'Parent Office',
            ),
            'public' => true,
            'publicly_queryable' => true,
            'menu_position' => 11,
            'supports' => array( 'title', 'editor', 'comments', 'custom-fields' ),
            //'taxonomies' => array( 'category' ),
            'menu_icon' => 'dashicons-flag',
            'has_archive' => true,
            'show_in_rest'       => true,
            'rest_base'          => 'events-api',
            'rest_controller_class' => 'WP_REST_Posts_Controller'
        )
    );
}


//removes the ability to edit office slugs, since they aren't their own pages
add_action('admin_head', 'custom_admin_css_offices');

function custom_admin_css_offices() {
  echo '<style>.post-type-office #edit-slug-box {display:none}</style>';
}
