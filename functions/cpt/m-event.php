<?php

//http://v2.wp-api.org/extending/custom-content-types/


add_action( 'init', 'create_events_taxonomy' );

function create_events_taxonomy() {
	$labels = array(
		'name'                       => 'Event Types',
		'singular_name'              => 'Event Type',
		'search_items'               => 'Search Event Types',
		'all_items'                  => 'All Event Types',
		'edit_item'                  => 'Edit Event Type',
		'update_item'                => 'Update Event Type',
		'add_new_item'               => 'Add New Event Type',
		'new_item_name'              => 'New Event Type Name',
		'menu_name'                  => 'Event Types',
		'view_item'                  => 'View Event Type',
		'popular_items'              => 'Popular Event Types',
		'separate_items_with_commas' => 'Separate event types with commas',
		'add_or_remove_items'        => 'Add or remove event types',
		'choose_from_most_used'      => 'Choose from the most used event types',
		'not_found'                  => 'No user event type found',
	);

	register_taxonomy(
		'event_types',
		'mobilize_event',
		array(
			'label'              => __( 'Event Types' ),
			'hierarchical'       => false,
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_in_nav_menus'  => false,
			'show_tagcloud'      => false,
			'show_admin_column'  => true,
		)
	);
}


add_action( 'init', 'cpt_m_event' );
function cpt_m_event() {
	register_post_type(
		'mobilize_event',
		array(
			'labels'              => array(
				'name'               => 'Mobilize Events',
				'singular_name'      => 'Event',
				'add_new'            => 'Add New',
				'add_new_item'       => 'Add New Event',
				'edit'               => 'Edit',
				'edit_item'          => 'Edit Events',
				'new_item'           => 'New Event',
				'view'               => 'View',
				'view_item'          => 'View Event',
				'search_items'       => 'Search Events',
				'not_found'          => 'No Events found',
				'not_found_in_trash' => 'No Events found in Trash',
				'parent'             => 'Parent Event',
			),
			'public'              => false,
			'menu_position'       => 11,
			'show_in_rest'        => true,
			'publicly_queryable'  => false,
			'exclude_from_search' => true,
			'show_ui'             => true,
			'show_in_nav_menus'   => true,
			'supports'            => array( 'title', 'editor', 'custom-fields' ),
			'taxonomies'          => array( 'event_types' ),
			'menu_icon'           => 'dashicons-tickets',
			'has_archive'         => false,
			'rewrite'             => false,
		)
	);
}

function add_before_editor( $post ) {
	if ( 'mobilize_event' == $post->post_type ) {
		remove_post_type_support( 'mobilize_event', 'editor' );
		do_meta_boxes( 'mobilize_event', 'read_only_content', $post );
	}
}
add_action( 'edit_form_after_title', 'add_before_editor' );

// add a box the location just created
function read_only_content_box() {
	add_meta_box(
		'read_only_content_box', // id, used as the html id att
		__( 'Full Description (Read Only)' ), // meta box title
		'read_only_cb', // callback function, spits out the content
		'mobilize_event', // post type or page. This adds to posts only
		'read_only_content', // context, where on the screen
		'low' // priority, where should this go in the context
	);
}
function read_only_cb( $post ) {
	$content = $post->post_content;

	$mobid     = get_post_meta( $post->ID, 'mobilize_id', true );
	$timeslots = get_post_meta( $post->ID, 'timeslots', true );
	$timezone  = get_post_meta( $post->ID, 'timezone', true );
	$timezone  = $timezone ? $timezone : 'America/Chicago';
	$venue     = get_post_meta( $post->ID, 'venue', true );
	$address   = get_post_meta( $post->ID, 'address', true );

	$timeslots_html = '<br><br>';
	if ( $timeslots && count( $timeslots ) ) {
		$last_day = '';
		foreach ( $timeslots as $timeslot ) {

			  $start_datestamp = new DateTime();
			  $end_datestamp   = new DateTime();

			  $start_datestamp->setTimezone( new DateTimeZone( $timezone ) );
			  $end_datestamp->setTimezone( new DateTimeZone( $timezone ) );

			  $start_datestamp->setTimestamp( $timeslot['start_date'] );
			  $end_datestamp->setTimestamp( $timeslot['end_date'] );

			$start_day = date( 'njY', $timeslot['start_date'] );
			$end_day   = date( 'njY', $timeslot['end_date'] );
			if ( $start_day != $last_day ) {
				$timeslots_html = $timeslots_html . '<br><br>' . date( 'F jS, Y', $timeslot['start_date'] ) . '<br>&nbsp;&nbsp;&nbsp;&nbsp;';
			} else {
				$timeslots_html = $timeslots_html . '<br>&nbsp;&nbsp;&nbsp;&nbsp;';
			}
			$timeslots_html = $timeslots_html . $start_datestamp->format( 'g:ia' ) . ' - ' .
			(
				$start_day != $end_day ?
					date( 'F jS, Y', $timeslot['end_date'] ) :
					''
			) . $end_datestamp->format( 'g:ia' );
			$last_day       = $end_day;
		}
	}
	$location = '<br><br>' . $venue . '<br>' . $address;

	echo apply_filters( 'the_content', $post->post_content . $location . $timeslots_html );
	echo '<span style="float:right">Mobilize Id: ' . $mobid . '</span><br>';
}
add_action( 'add_meta_boxes', 'read_only_content_box' );


//remove YOAST
function my_remove_wp_seo_meta_box() {
	remove_meta_box( 'wpseo_meta', 'mobilize_event', 'normal' );
	remove_meta_box( 'aam-acceess-manager', 'mobilize_event', 'advanced' );
}
add_action( 'add_meta_boxes', 'my_remove_wp_seo_meta_box', 100 );


//Add new columns in the back end for this post type.
// add_filter( 'manage_edit-mobilize_event_columns', 'wec_mobilize_event_columns' );
// function wec_mobilize_event_columns( $columns ) {
//     $columns['start_time'] = __( 'Start Date', 'we-mobilize' );
//     return $columns;
// }

// // Populate new columns with specific field values.
// add_action( 'manage_mobilize_event_posts_custom_column', 'wec_mobilize_event_column_content', 10, 2 );
// function wec_mobilize_event_column_content( $column_name, $post_id ) {
//     if ( 'start_time' != $column_name ) {
//         return;
//     }
//     $date = get_field( 'event_start_time', $post_id, false );
//     $date = new DateTime( $date );
//     echo $date->format( 'Y/m/d' );
// }

// // Make new columns sortable.
// add_filter( 'manage_edit-mobilize_event_sortable_columns', 'wec_sortable_mobilize_event_column' );
// function wec_sortable_mobilize_event_column( $columns ) {
//     $columns['start_time'] = 'start_time';
//     return $columns;
// }


//location
//Add new columns in the back end for this post type.
add_filter( 'manage_edit-mobilize_event_columns', 'wec_mobilize_event_columns' );
function wec_mobilize_event_columns( $columns ) {
	$columns['event_days'] = __( 'Event Days', 'we-mobilize' );
	$columns['citystate']  = __( 'Event Location', 'we-mobilize' );
	unset( $columns['wpseo-score-readability'] );
	unset( $columns['wpseo-score'] );
	unset( $columns['wpseo-links'] );
	return $columns;
}

// Populate new columns with specific field values.
add_action( 'manage_mobilize_event_posts_custom_column', 'wec_mobilize_event_column_content', 10, 2 );
function wec_mobilize_event_column_content( $column_name, $post_id ) {

	if ( 'citystate' == $column_name ) {
		$citystate = get_field( 'citystate', $post_id, false );
		echo $citystate;
	} elseif ( 'event_days' == $column_name ) {
		$timeslots = get_field( 'timeslots', $post_id, false );

		$start_date = '';
		$end_date   = '';

		if ( $timeslots && count( $timeslots ) ) {
			foreach ( $timeslots as $timeslot ) {
				if ( $start_date == '' ) {
					$start_date = date( 'F jS, Y', $timeslot['start_date'] );
				} elseif ( $start_date != date( 'F jS, Y', $timeslot['start_date'] ) ) {
					$end_date = ' - ' . date( 'F jS, Y', $timeslot['end_date'] );
				}
			}
		}
		echo $start_date . $end_date;
	} else {
		return;
	}
}

// Make new columns sortable.
add_filter( 'manage_edit-mobilize_event_sortable_columns', 'wec_sortable_mobilize_event_column' );
function wec_sortable_mobilize_event_column( $columns ) {
	$columns['event_days'] = 'event_days';
	return $columns;
}


add_action( 'pre_get_posts', 'my_slice_orderby' );
function my_slice_orderby( $query ) {
	if ( ! is_admin() ) {
		return;
	}

	$orderby = $query->get( 'orderby' );

	if ( 'event_days' == $orderby ) {
		$query->set( 'meta_key', 'first_day' );
		$query->set( 'orderby', 'first_day' );
	}
}


function rewrite_cpt_header() {
	$screen = get_current_screen();
	if ( $screen->id != 'edit-mobilize_event' ) {
		return;
	} else {
			$mobilize = new Mobilize_Sync();
			$org      = $mobilize->mobilize_get_org();
			$title    = isset( $org['name'] ) ? $org['name'] : '';
		?>
		<div class="wrap">
		<h1 class="wp-heading-inline show" style="display:inline-block;"><?php echo $title; ?></h1>
		</div>

		<style scoped>
/*                .wp-heading-inline:not(.show),
		.page-title-action:not(.show) { display:none !important;}*/
		</style>
		<?php
	}
}
 add_action( 'admin_notices', 'rewrite_cpt_header' );
