<?php
if( function_exists('acf_add_local_field_group') ):

$office_acf_fields =  array(
  'key' => 'group_5b2bfcaa1da5c',
  'title' => 'Office Details',
  'fields' => array(
    array(
      'key' => 'field_5b479a2cc3d25',
      'label' => 'Excerpt',
      'name' => 'office_excerpt',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
    ),
    array(
      'key' => 'field_5b2bfcaa32843',
      'label' => 'Office Phone',
      'name' => 'office_phone_number',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
    ),
    array(
      'key' => 'field_5b2bfcaa3290e',
      'label' => 'Description',
      'name' => 'office_content',
      'type' => 'wysiwyg',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'tabs' => 'all',
      'toolbar' => 'full',
      'media_upload' => 1,
      'delay' => 0,
    ),
    array(
      'key' => 'field_5b2bfcaa328cd',
      'label' => 'Location',
      'name' => 'office_location',
      'type' => 'google_map',
      'instructions' => '',
      'required' => 1,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'center_lat' => '',
      'center_lng' => '',
      'zoom' => '',
      'height' => '',
    ),
  ),
  'location' => array(
    array(
      array(
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'office',
      ),
    ),
  ),
  'menu_order' => 0,
  'position' => 'acf_after_title',
  'style' => 'default',
  'label_placement' => 'top',
  'instruction_placement' => 'label',
  'hide_on_screen' => array(
    0 => 'the_content',
  ),
  'active' => 1,
  'description' => '',
);

  acf_add_local_field_group($office_acf_fields);//json allows you to edit options in ACF admin, but doesn't really work for something added by plugin


endif;