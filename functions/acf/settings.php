<?php



function acf_field_group_to_json( $array ) {

	$array['modified'] = time();
	$group             = json_encode( $array, JSON_PRETTY_PRINT );
	//echo '<p>Writing file to ' . $array['key'] . '.json</p>';
	//$file = file_put_contents( get_template_directory(). '/acf-json/'.$array['key'] . '.json', $group );
	echo $group;
	exit();
	//return $file = file_put_contents( dirname(__FILE__) . '/'.$array['key'] . '.json', $group );
}

if ( function_exists( 'acf_add_local_field_group' ) ) :

	$mobilize_acf_fields = array(
		'key'                   => 'group_5aedec0252b12',
		'title'                 => 'Mobilize Events Settings',
		'fields'                => array(
			array(
				'key'               => 'field_5af1f9bd41dc2',
				'label'             => 'API Settings',
				'name'              => '',
				'type'              => 'tab',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'placement'         => 'top',
				'endpoint'          => 0,
			),
			array(
				'key'               => 'field_5aedec091913b',
				'label'             => 'Organization ID',
				'name'              => 'mobilize_organization_id',
				'type'              => 'number',
				'instructions'      => 'Your Mobilize Organization ID. Warning: changing this ID will purge and resync all events.',
				'required'          => 1,
				'conditional_logic' => array(
					array(
						array(
							'field'    => 'field_8a71a5b1gf04c',
							'operator' => '==',
							'value'    => 'api',
						),
					),
				),
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => 'Org ID',
				'prepend'           => '',
				'append'            => '',
				'min'               => '',
				'max'               => '',
				'step'              => '',
			),
			array(
				'key'               => 'field_5af1a5b1af0dc',
				'label'             => 'Sync w/ Mobilize API',
				'name'              => 'mobilize_sync_setting',
				'type'              => 'button_group',
				'instructions'      => '',
				'required'          => 1,
				'conditional_logic' => array(
					array(
						array(
							'field'    => 'field_8a71a5b1gf04c',
							'operator' => '==',
							'value'    => 'api',
						),
					),
				),
				'wrapper'           => array(
					'width' => '50',
					'class' => '',
					'id'    => '',
				),
				'choices'           => array(
					'off'        => 'Off',
					'hourly'     => 'Hourly',
					'twicedaily' => 'Twice Daily',
					'daily'      => 'Daily',
				),
				'allow_null'        => 0,
				'default_value'     => 'twicedaily',
				'layout'            => 'horizontal',
				'return_format'     => 'value',
			),
			array(
				'key'               => 'field_5af34e08c475c',
				'label'             => 'Which Events Should Sync?',
				'name'              => 'which_events_should_sync',
				'type'              => 'button_group',
				'instructions'      => '',
				'required'          => 1,
				'conditional_logic' => array(
					array(
						array(
							'field'    => 'field_8a71a5b1gf04c',
							'operator' => '==',
							'value'    => 'api',
						),
					),
				),
				'wrapper'           => array(
					'width' => '50',
					'class' => '',
					'id'    => '',
				),
				'choices'           => array(
					'all'    => 'All Events',
					'future' => 'Only Future Events',
				),
				'allow_null'        => 0,
				'default_value'     => 'future',
				'layout'            => 'horizontal',
				'return_format'     => 'value',
			),
			array(
				'key'               => 'field_5b19ddb5b92d2',
				'label'             => 'No Events Message',
				'name'              => 'no_events_message',
				'type'              => 'text',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => 'Sorry, no upcoming events at this time.',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => 200,
			),
			array(
				'key'               => 'field_5aff38d6d8196',
				'label'             => 'Show Images?',
				'name'              => 'show_mobilize_images',
				'type'              => 'button_group',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '50',
					'class' => '',
					'id'    => '',
				),
				'choices'           => array(
					'no'  => 'No',
					'yes' => 'Yes',
				),
				'allow_null'        => 0,
				'default_value'     => 'no',
				'layout'            => 'horizontal',
				'return_format'     => 'value',
			),
			array(
				'key'               => 'field_5af1e867c8293',
				'label'             => 'Advanced',
				'name'              => '',
				'type'              => 'tab',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'placement'         => 'top',
				'endpoint'          => 0,
			),
			array(
				'key'               => 'field_8a71a5b1gf04c',
				'label'             => 'Mobilize Mode',
				'name'              => 'mobilize_mode',
				'type'              => 'button_group',
				'instructions'      => 'Whether or not to use synchronized WP data to serve event data or static files.',
				'required'          => 1,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '50',
					'class' => '',
					'id'    => '',
				),
				'choices'           => array(
					'api'    => 'API Sync',
					'static' => 'Static files',
				),
				'allow_null'        => 0,
				'default_value'     => 'api',
				'layout'            => 'horizontal',
				'return_format'     => 'value',
			),
			array(
				'key'               => 'field_8b71a6b1df04a',
				'label'             => 'Virtual Scroll',
				'name'              => 'mobilize_virtual_scroll',
				'type'              => 'button_group',
				'instructions'      => 'Whether the scrolling list of events is virtualized or not: necessary if the number of events is over a few hundred',
				'required'          => 1,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '50',
					'class' => '',
					'id'    => '',
				),
				'choices'           => array(
					'false' => 'Off',
					'true'  => 'On',
				),
				'allow_null'        => 0,
				'default_value'     => 'false',
				'layout'            => 'horizontal',
				'return_format'     => 'value',
			),
			array(
				'key'               => 'field_5b34e90f8158a',
				'label'             => 'Mobilize Endpoint',
				'name'              => 'mobilize_endpoint',
				'type'              => 'select',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => array(
					array(
						array(
							'field'    => 'field_8a71a5b1gf04c',
							'operator' => '==',
							'value'    => 'api',
						),
					),
				),
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'choices'           => array(
					'all_orgs' => 'api/v1/organizations/:organization_id/events',
					'one_org'  => 'api/v1/events',
				),
				'default_value'     => array(
					0 => 'one_org',
				),
				'allow_null'        => 0,
				'multiple'          => 0,
				'ui'                => 0,
				'ajax'              => 0,
				'return_format'     => 'value',
				'placeholder'       => '',
			),
			array(
				'key'               => 'field_6ahde3421y13b',
				'label'             => 'Static Endpoint',
				'name'              => 'mobilize_static_endpoint',
				'type'              => 'text',
				'instructions'      => 'Relative path to static files (all.json, map.json, events/[ID].json)',
				'required'          => 1,
				'conditional_logic' => array(
					array(
						array(
							'field'    => 'field_8a71a5b1gf04c',
							'operator' => '!=',
							'value'    => 'api',
						),
					),
				),
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '/mobilize/',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => '',
			),
			array(
				'key'               => 'field_5b3240268c62a',
				'label'             => 'Map Styles',
				'name'              => 'mobilize_map_styles',
				'type'              => 'textarea',
				'instructions'      => 'Optional: customize the map styles. A valid json google styles object required (Warning: Entering invalid data here could disable the map)',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'maxlength'         => '',
				'rows'              => '8',
				'new_lines'         => '',
			),
			array(
				'key'               => 'field_5b3270278c63a',
				'label'             => 'Map Bounds',
				'name'              => 'mobilize_map_bounds',
				'type'              => 'textarea',
				'instructions'      => 'Optional: this will force the default map view to a specific set of bounds. A valid json bounds object is required (Warning: Entering invalid data here could disable the map)',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '70',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => 'Example: {"ne":{"lat":49.5388001,"lng":-66.885417},"sw":{"lat":25.7763,"lng":-124.5957}}',
				'maxlength'         => '',
				'rows'              => '2',
				'new_lines'         => '',
			),
			array(
				'key'               => 'field_670680a1-d2f7-4f19-9332-3ab57a98c62b',
				'label'             => 'Default Zoom',
				'name'              => 'mobilize_default_zoom',
				'type'              => 'range',
				'instructions'      => 'Set the zoom level of the map when it first loads. 0 is the whole world.',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '30',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => 4,
				'min'               => 0,
				'max'               => 8,
				'step'              => '',
				'prepend'           => '',
				'append'            => '',
			),
			array(
				'key'               => 'field_5bc9de0fc8a80',
				'label'             => 'Minimum Zoom',
				'name'              => 'mobilize_minimum_zoom',
				'type'              => 'range',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '30',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => 4,
				'min'               => 0,
				'max'               => 8,
				'step'              => '',
				'prepend'           => '',
				'append'            => '',
			),
			array(
				'key'               => 'field_8jed6c422113e',
				'label'             => 'Source parameter string',
				'name'              => 'mobilize_sourcing',
				'type'              => 'text',
				'instructions'      => 'List of parameters, separated by &, to append to outbound links to mobilize events.',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => 'utm_source=CLIENT&utm_medium=website&utm_campaign=YOURCAMPAIGN',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => '',
			),
			array(
				'key'               => 'field_5aedec421913c',
				'label'             => 'Mobilize API Key',
				'name'              => 'mobilize_api_key',
				'type'              => 'text',
				'instructions'      => 'Currently not necessary.',
				'required'          => 0,
				'conditional_logic' => array(
					array(
						array(
							'field'    => 'field_8a71a5b1gf04c',
							'operator' => '==',
							'value'    => 'api',
						),
					),
				),
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => '',
			),
			array(
				'key'               => 'field_5af1e8cfc8294',
				'label'             => '',
				'name'              => '',
				'type'              => 'message',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'message'           => '<a href="/wp-admin/admin.php?page=mobilize-sync">Sync</a>',
				'new_lines'         => '',
				'esc_html'          => 0,
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'options_page',
					'operator' => '==',
					'value'    => 'mobilize',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => 1,
		'description'           => '',
	);

	//if (!file_exists(get_template_directory(). '/acf-json/'.$mobilize_acf_fields['key'] . '.json')) {
	// if (!file_exists(get_template_directory(). '/acf-json/'.$mobilize_acf_fields['key'] . '.json')) {
	//   echo acf_field_group_to_json($mobilize_acf_fields);
	//   exit();
	// }

	// add_filter('acf/settings/load_json', function($paths) {
	//     $paths[] = dirname(__FILE__);
	//     return $paths;
	// });

	acf_add_local_field_group( $mobilize_acf_fields );//json allows you to edit options in ACF admin, but doesn't really work for something added by plugin
	//acf_field_group_to_json($mobilize_acf_fields);



	add_filter( 'acf/validate_value/name=mobilize_map_styles', 'acf_is_valid_json', 10, 4 );
	add_filter( 'acf/validate_value/name=mobilize_map_bounds', 'acf_is_valid_json', 10, 4 );

	function acf_is_valid_json( $valid, $value, $field, $input ) {

		// bail early if value is already invalid
		if ( ! $valid ) {
			return $valid;
		}

		$start_type = gettype( $value );
		$end_type   = gettype( @json_decode( stripslashes( $value ) ) );

		if ( $value != '' && ( $end_type != 'object' && $end_type != 'array' ) ) {
			$valid = "{$value} decoded to {$end_type}, Invalid JSON";
		}

		// return
		return $valid;

	}


endif;
