<?php
/*
Plugin Name: Wide Eye Mobilize Events Integration
Plugin URI: https://www.wideeyecreative.com/
Description: Plugin created for Wide Eye Creative starter theme
Version: 1.0.1
Author: Wide Eye
Author URI: https://www.wideeyecreative.com/
License: GPLv2
*/

// if ( ! defined( 'WECCORE_FILE' ) ) {
//     define( 'WECCORE_FILE', __FILE__ );
// }
// if ( ! defined( 'WECCORE_PATH' ) ) {
//     define( 'WECCORE_PATH', plugin_dir_path( WECCORE_FILE ) );
// }

// Custom Post Types

if ( function_exists( 'acf_add_local_field_group' ) ) {
	require_once 'functions/acf/settings.php';
	// require_once 'functions/cpt/offices.php';
	require_once 'functions/acf/offices.php';
	require_once 'functions/cpt/m-event.php';
	require_once 'functions/features/cron.php';
	require_once 'functions/features/options.php';
}


/* functions */

function setCacheHeaders( $seconds_to_cache = 3600 ) {
	$ts = gmdate( 'D, d M Y H:i:s', time() + $seconds_to_cache ) . ' GMT';
	header( "Expires: $ts" );
	header( "X-TEST: ${seconds_to_cache}" );
	header( 'Pragma: cache' );
	header( 'Cache-Control: max-age=' . $seconds_to_cache . ', public' );
	header( 'Surrogate-Control: max-age=' . $seconds_to_cache . ', stale-while-revalidate=86400, stale-if-error=86400' );
}

/*gets a single event*/
function mobilize_get_full_event() {
	header( 'Content-Type: application/json' );
	$event_post_id = $_GET['post_id'];

	if ( ! isset( $event_post_id ) ) {
		return new WP_Error( 'mobilize_no_post_id', 'Invalid post_id', array( 'status' => 404 ) );
		exit();
	}

	$event = get_post( intval( $event_post_id ) );

	if ( ! isset( $event ) && strpos( $event_post_id, 'm' ) !== false ) {

		$args  = array(
			'post_type'      => 'mobilize_event',
			'posts_per_page' => 1,
			'meta_query'     => array(
				array(
					'key'     => 'mobilize_id',
					'value'   => intval( preg_replace( '~\D~', '', $event_post_id ) ),
					'compare' => '=',
				),
			),
		);
		$query = get_posts( $args );

		if ( ! $query || ! array_key_exists( 0, $query ) ) {
			return new WP_Error( 'mobilize_no_event_found', 'Event not found', array( 'status' => 404 ) );
			exit();
		} else {
			$event = $query[0];
		}
	}

	$json = null;

	$raw_json = get_fields( $event->ID );

	if ( $event->post_type == 'mobilize_event' ) {
		$json                     = get_post_meta( $event->ID, 'mobilize_json', true );
		$json                     = json_decode( $json, true );
		$json['description']      = $json['description'];
		$json['latitude']         = get_post_meta( $event->ID, 'lat', true );
		$json['longitude']        = get_post_meta( $event->ID, 'lng', true );
		$json['post_id']          = $event->ID;
		$json['event_start_time'] = $raw_json['event_start_time'];
		$json['first_day']        = $raw_json['first_day'];
	} elseif ( $event->post_type == 'event' ) {
		$json = array();

		$json['source_type'] = $event->post_type;
		$json['post_id']     = $event->ID;
		$json['is_wp_item']  = true;
		$json['title']       = $event->post_title;
		$slug                = $event->post_name;
		$json['slug']        = $slug;
		if ( array_key_exists( 'map', $raw_json ) ) {
			$map_field = $raw_json['map'];
			if ( $map_field ) {
				$json['latitude']  = $map_field['lat'];
				$json['longitude'] = $map_field['lng'];
				$json['location']  = array(
					'venue'        => $raw_json['venue'],
					'location'     => array(
						'latitude'  => $map_field['lat'],
						'longitude' => $map_field['lng'],
					),
					'full_address' => $map_field['address'],
				);
			}
		}

		$start_time = $raw_json['event_start_time'];
		if ( $start_time ) {
			$start_time = strtotime( $start_time );
		}
		$end_time = $raw_json['event_end_time'];

		if ( $end_time ) {
			$end_time = strtotime( $end_time );
		} else {
			$end_time = $start_time;
		}
		$rsvp_link            = get_field( 'rsvp_link', $event->ID );
		$json['browser_url']  = $rsvp_link ? $rsvp_link : "/event/{$slug}";
		$json['local_url']    = "/event/{$slug}";
		$json['has_rsvp_url'] = (bool) $rsvp_link;
		$json['description']  = str_replace( array( '&#8217;', '&#8220;', '&#8221;' ), array( "'", '"', '"' ), strip_tags( $raw_json['event_content'] ) );
		$json['timeslots']    = array(
			array(
				'start_date' => $start_time,
				'end_date'   => $end_time,
			),
		);
	} elseif ( $event->post_type == 'office' ) {
		$json                = array();
		$json['source_type'] = $event->post_type;
		$json['post_id']     = $event->ID;
		$json['is_wp_item']  = true;
		$json['is_poi']      = true;
		$json['title']       = $event->post_title;
		$slug                = $event->post_name;
		$json['slug']        = $slug;

		$json['excerpt']             = get_post_meta( $event->ID, 'office_excerpt', true );
		$json['office_phone_number'] = get_post_meta( $event->ID, 'office_phone_number', true );
		$json['description']         = $raw_json['office_content'];
		$map_field                   = $raw_json['office_location'];
		if ( $map_field ) {
			$json['latitude']  = $map_field['lat'];
			$json['longitude'] = $map_field['lng'];
			$json['location']  = array(
				'venue'        => '',
				'location'     => array(
					'latitude'  => $map_field['lat'],
					'longitude' => $map_field['lng'],
				),
				'full_address' => $map_field['address'],
			);
		}
	}

	if ( ! isset( $json ) ) {
		return new WP_Error( 'mobilize_no_event_found', 'Event not found', array( 'status' => 404 ) );
		exit();
	}
	setCacheHeaders();
	return $json;

}

/*gets offices*/
function mobilize_get_offices() {
	header( 'Content-Type: application/json' );
	//delete_transient( 'we-mobilize-offices' );
	$offices_json = get_transient( 'we-mobilize-offices' );

	if ( false === $offices_json ) {
		$offices_json = array();
		$offices      = get_posts(
			array(
				'numberposts' => -1,
				'post_type'   => 'office',
			)
		);

		foreach ( $offices as $office ) {
			$json                        = array();
			$json['source_type']         = $office->post_type;
			$json['post_id']             = $office->ID;
			$json['is_wp_item']          = true;
			$json['is_poi']              = true;
			$json['title']               = $office->post_title;
			$slug                        = $office->post_name;
			$json['slug']                = $slug;
			$map_field                   = get_post_meta( $office->ID, 'office_location', true );
			$json['excerpt']             = get_post_meta( $office->ID, 'office_excerpt', true );
			$json['office_phone_number'] = get_post_meta( $office->ID, 'office_phone_number', true );
			$json['latitude']            = $map_field['lat'];
			$json['longitude']           = $map_field['lng'];
			$json['description']         = get_post_meta( $office->ID, 'office_content', true );
			$json['location']            = array(
				'venue'        => '',
				'location'     => array(
					'latitude'  => $map_field['lat'],
					'longitude' => $map_field['lng'],
				),
				'full_address' => $map_field['address'],
			);
			array_push( $offices_json, $json );
		}
		set_transient( 'we-mobilize-offices', $offices_json, 3600 );
	}
	setCacheHeaders();
	return $offices_json;
}


function mobilize_get_offices_static() {
	//delete_transient( 'we-mobilize-offices' );
	$offices_json = get_transient( 'we-mobilize-offices' );

	if ( false === $offices_json ) {
		$offices_json = array();
		$offices      = get_posts(
			array(
				'numberposts' => -1,
				'post_type'   => 'office',
			)
		);

		foreach ( $offices as $office ) {
			$json                        = array();
			$json['source_type']         = $office->post_type;
			$json['post_id']             = $office->ID;
			$json['is_wp_item']          = true;
			$json['is_poi']              = true;
			$json['title']               = $office->post_title;
			$slug                        = $office->post_name;
			$json['slug']                = $slug;
			$map_field                   = get_post_meta( $office->ID, 'office_location', true );
			$json['excerpt']             = get_post_meta( $office->ID, 'office_excerpt', true );
			$json['office_phone_number'] = get_post_meta( $office->ID, 'office_phone_number', true );
			$json['latitude']            = $map_field['lat'];
			$json['longitude']           = $map_field['lng'];
			$json['description']         = get_post_meta( $office->ID, 'office_content', true );
			$json['location']            = array(
				'venue'        => '',
				'location'     => array(
					'latitude'  => $map_field['lat'],
					'longitude' => $map_field['lng'],
				),
				'full_address' => $map_field['address'],
			);
			array_push( $offices_json, $json );
		}
		set_transient( 'we-mobilize-offices', $offices_json, 3600 );
	}
	return $offices_json;
}


/*shortform events*/
function mobilize_get_all_map_events_short() {
	header( 'Content-Type: application/json' );
	//delete_transient( 'we-mobilize-short' );
	$events_map = get_transient( 'we-mobilize-short' );
	//$events_map = false;

	$post_types = array( 'mobilize_event' );

	$use_native_events = true;
	if ( $use_native_events ) {
		array_push( $post_types, 'event' );
	}

	$now    = time();
	$now_ds = current_time( 'Y-m-d' );

	if ( false === $events_map ) {
		$events_map    = array();
		$synced_events = get_posts(
			array(
				'numberposts' => -1,
				'post_type'   => $post_types,
				'meta_key'    => 'event_end_time',
				'orderby'     => 'meta_value',
				'order'       => 'ASC',
				'meta_query'  => array(
					'key'     => 'event_end_time',
					'value'   => $now_ds,
					'type'    => 'DATETIME',
					'compare' => '>=',
				),
			)
		);

		$events_map = array();

		foreach ( $synced_events as $event ) {

			if ( $event->post_type == 'mobilize_event' ) {
				$lat = get_post_meta( $event->ID, 'lat', true );
				$lng = get_post_meta( $event->ID, 'lng', true );
				//$zip = get_post_meta($event->ID, 'zip', true);
				if ( ! $lat || ! $lng ) {
					continue;
				}
			} elseif ( $event->post_type == 'event' ) {
				$raw_json = get_fields( $event->ID );
				if ( ! array_key_exists( 'map', $raw_json ) ) {
					continue;
				}
				$map_field = $raw_json['map'];
				if ( ! $map_field || ! array_key_exists( 'lat', $map_field ) ) {
					continue;
				}

				$lat = $map_field['lat'];
				$lng = $map_field['lng'];
			}

			array_push(
				$events_map,
				array(
					'latitude'  => $lat,
					'longitude' => $lng,
					//"zip" => $zip,
					'post_id'   => $event->ID,
					'title'     => $event->post_title,
				)
			);
		}
		set_transient( 'we-mobilize-short', $events_map, 3600 );
	}
	setCacheHeaders();
	return $events_map;
}


/*shortform events*/
function mobilize_get_native_map_events() {
	header( 'Content-Type: application/json' );
	delete_transient( 'we-mobilize-native' );
	$events_map = get_transient( 'we-mobilize-native' );
	//$events_map = false;

	$post_types = array( 'event' );

	$now    = time();
	$now_ds = current_time( 'Y-m-d' );

	if ( false === $events_map ) {
		$events_map    = array();
		$synced_events = get_posts(
			array(
				'numberposts' => -1,
				'post_type'   => $post_types,
				'meta_key'    => 'event_end_time',
				'orderby'     => 'meta_value',
				'order'       => 'ASC',
				'meta_query'  => array(
					'key'     => 'event_end_time',
					'value'   => $now_ds,
					'type'    => 'DATETIME',
					'compare' => '>=',
				),
			)
		);

		$events_map = array();

		foreach ( $synced_events as $event ) {

			if ( $event->post_type == 'event' ) {
				$raw_json           = get_fields( $event->ID );
				$json['is_wp_item'] = true;
				$json['title']      = $event->post_title;
				$slug               = $event->post_name;

				//var_dump($event);
				$json['slug']    = $slug;
				$json['post_id'] = $event->ID;
				$category        = get_the_terms( $event->ID, 'event_types' );
				if ( $category && isset( $category ) && ! is_wp_error( $category ) && $category[0]->name !== 'Events' ) {
					$json['event_type'] = $category[0]->name;
				}

				if ( array_key_exists( 'map', $raw_json ) ) {
					$map_field = $raw_json['map'];
					if ( $map_field ) {
						  $json['latitude']  = $map_field['lat'];
						  $json['longitude'] = $map_field['lng'];
						  $json['location']  = array(
							  'venue'        => $raw_json['venue'],
							  'location'     => array(
								  'latitude'  => $map_field['lat'],
								  'longitude' => $map_field['lng'],
							  ),
							  'full_address' => $map_field['address'],
						  );
					}
				}

				$start_time = $raw_json['event_start_time'];
				if ( $start_time ) {
					$start_time = strtotime( $start_time );
				}
				$end_time = $raw_json['event_end_time'];

				if ( $end_time ) {
						$end_time = strtotime( $end_time );
				} else {
					$end_time = $start_time;
				}

				if ( $end_time < $now - 10000 ) {
							continue;
				}
				$rsvp_link                = get_field( 'rsvp_link', $event->ID );
				$json['browser_url']      = $rsvp_link ? $rsvp_link : "/event/{$slug}";
				$json['has_rsvp_url']     = (bool) $rsvp_link;
				$json['local_url']        = "/event/{$slug}";
				$json['description']      = str_replace( array( '&#8217;', '&#8220;', '&#8221;' ), array( "'", '"', '"' ), strip_tags( $raw_json['event_content'] ) );
				$json['timeslots']        = array(
					array(
						'start_date' => $start_time,
						'end_date'   => $end_time,
					),
				);
				$json['first_day']        = $start_time;
				$json['event_start_time'] = $raw_json['event_start_time'];
			}
			array_push( $events_map, $json );
		}
		set_transient( 'we-mobilize-native', $events_map, 3600 );
	}
	setCacheHeaders();
	return $events_map;
}


/*all events, all data*/
function mobilize_get_all_map_events_long() {
	header( 'Content-Type: application/json' );
	//delete_transient( 'we-mobilize-long-2' );
	$events_map = get_transient( 'we-mobilize-long-2' );
	//$events_map = false;

	$post_types = array( 'mobilize_event' );

	$use_native_events = true;
	if ( $use_native_events ) {
		array_push( $post_types, 'event' );
	}

	$now    = time();
	$now_ds = current_time( 'Y-m-d' );

	if ( false === $events_map ) {
		$events_map    = array();
		$synced_events = get_posts(
			array(
				'numberposts' => -1,
				'post_type'   => $post_types,
				'meta_key'    => 'event_end_time',
				'orderby'     => 'meta_value',
				'order'       => 'ASC',
				'meta_query'  => array(
					'key'     => 'event_end_time',
					'value'   => $now_ds,
					'type'    => 'DATETIME',
					'compare' => '>=',
				),
			)
		);

		  // 'meta_query'  => array(
		  //   'relation'  => 'OR',
		  //   array(
		  //     'key'       => 'first_day',
		  //     'value'     => $now,
		  //     'type'      => 'NUMERIC',
		  //     'compare'   => '>'
		  //   ),
		  //   array(
		  //     'key'       => 'event_start_time',
		  //     'value'     => $now_ds,
		  //     'type'      => 'DATETIME',
		  //     'compare'   => '>'
		  //   )
		  // )

		foreach ( $synced_events as $event ) {
			$json                = get_post_meta( $event->ID, 'mobilize_json', true );
			$json                = json_decode( $json, true );
			$json['source_type'] = $event->post_type;
			$json['post_id']     = $event->ID;
			if ( $event->post_type == 'mobilize_event' ) {
				if ( array_key_exists( 'location', $json ) ) {
					$lat = get_post_meta( $event->ID, 'lat', true );
					$lng = get_post_meta( $event->ID, 'lng', true );
					if ( $lat && $lng ) {
						$json['latitude']  = $lat;
						$json['longitude'] = $lng;
					}
				}
				$json['first_day']        = get_post_meta( $event->ID, 'first_day', true );
				$json['event_start_time'] = get_post_meta( $event->ID, 'event_start_time', true );
			} elseif ( $event->post_type == 'event' ) {
				$raw_json           = get_fields( $event->ID );
				$json['is_wp_item'] = true;
				$json['title']      = $event->post_title;
				$slug               = $event->post_name;
				$json['slug']       = $slug;
				if ( array_key_exists( 'map', $raw_json ) ) {
					$map_field = $raw_json['map'];
					if ( $map_field ) {
						$json['latitude']  = $map_field['lat'];
						$json['longitude'] = $map_field['lng'];
						$json['location']  = array(
							'venue'        => $raw_json['venue'],
							'location'     => array(
								'latitude'  => $map_field['lat'],
								'longitude' => $map_field['lng'],
							),
							'full_address' => $map_field['address'],
						);
					}
				}

				$start_time = $raw_json['event_start_time'];
				if ( $start_time ) {
					$start_time = strtotime( $start_time );
				}
				$end_time = $raw_json['event_end_time'];

				if ( $end_time ) {
						$end_time = strtotime( $end_time );
				} else {
					$end_time = $start_time;
				}

				if ( $end_time < $now - 10000 ) {
							continue;
				}
				$rsvp_link                = get_field( 'rsvp_link', $event->ID );
				$json['browser_url']      = $rsvp_link ? $rsvp_link : "/event/{$slug}";
				$json['has_rsvp_url']     = (bool) $rsvp_link;
				$json['local_url']        = "/event/{$slug}";
				$json['description']      = str_replace( array( '&#8217;', '&#8220;', '&#8221;' ), array( "'", '"', '"' ), strip_tags( $raw_json['event_content'] ) );
				$json['timeslots']        = array(
					array(
						'start_date' => $start_time,
						'end_date'   => $end_time,
					),
				);
				$json['first_day']        = $start_time;
				$json['event_start_time'] = $raw_json['event_start_time'];
			}
			array_push( $events_map, $json );
		}

		if ( count( $events_map ) > 0 ) {
			set_transient( 'we-mobilize-long-2', $events_map, 3600 );
		}
	}

	setCacheHeaders();
	return $events_map;
}

function slug_get_post_meta_cb( $object, $field_name, $request ) {
	return get_post_meta( $object['id'], $field_name, true );
}

// function slug_update_post_meta_cb( $value, $object, $field_name ) {
//     return update_post_meta( $object[ 'id' ], $field_name, $value );
// }

add_action(
	'rest_api_init',
	function () {
		register_rest_route(
			'we-mobilize/v2',
			'/short/',
			array(
				'methods'  => 'GET',
				'callback' => 'mobilize_get_all_map_events_short',
			)
		);

		register_rest_route(
			'we-mobilize/v2',
			'/offices/',
			array(
				'methods'  => 'GET',
				'callback' => 'mobilize_get_offices',
			)
		);

		register_rest_route(
			'we-mobilize/v2',
			'/long/',
			array(
				'methods'  => 'GET',
				'callback' => 'mobilize_get_all_map_events_long',
			)
		);

		register_rest_route(
			'we-mobilize/v2',
			'/native/',
			array(
				'methods'  => 'GET',
				'callback' => 'mobilize_get_native_map_events',
			)
		);

		register_rest_route(
			'we-mobilize/v2',
			'/event/',
			array(
				'methods'  => 'GET',
				'callback' => 'mobilize_get_full_event',
			)
		);

		register_rest_field(
			'mobilize_event',
			'mobilize_json',
			array(
				'get_callback' => 'slug_get_post_meta_cb',
				///'update_callback' => 'slug_update_post_meta_cb',
				'schema'       => null,
			)
		);

	}
);


function mobilize_activate() {
	$mobilize = new Mobilize_Sync();
	$mobilize->get_orgs();
	// clear the permalinks after the post type has been registered
	flush_rewrite_rules();
}

register_activation_hook( __FILE__, 'mobilize_activate' );

function mobilize_deactivate() {
	global $wpdb;
	add_filter( 'wpseo_enable_notification_post_trash', '__return_false' );
	mobilize_purge_events();
	//delete_field('mobilize_organization_id', 'mobilize_event');
	$wpdb->get_results( "DELETE FROM wp_terms WHERE term_id IN ( SELECT * FROM ( SELECT wp_terms.term_id FROM wp_terms JOIN wp_term_taxonomy ON wp_term_taxonomy.term_id = wp_terms.term_id WHERE taxonomy = 'event_types' AND count = 0 ) as T)" );

	flush_rewrite_rules();
}
register_deactivation_hook( __FILE__, 'mobilize_deactivate' );

function printMobilizeWidget() {
	if ( ! is_plugin_active( 'we-mobilize/we-mobilize.php' ) ) {
		return '';
	}
	$site_url = parse_url( site_url() )['host'];
	if ( isset( $_SERVER['HTTPS'] ) &&
	  ( $_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1 ) ||
	  isset( $_SERVER['HTTP_X_FORWARDED_PROTO'] ) &&
	  $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' ) {
		$protocol = 'https://';
	} else {
		$protocol = 'http://';
	}
	$description              = get_field( 'introduction_description' );
	$description              = str_replace( array( "\n", "\t", "\r" ), ' ', strip_tags( $description ) );
	$title                    = get_field( 'introduction_headline' );
	$title                    = $title ? $title : get_the_title();
	$title                    = str_replace( array( "\n", "\t", "\r" ), ' ', $title );
	$site_url                 = stripos( $site_url, 'http' ) === true ? $site_url : ( $protocol . $site_url );
	$mapStyles                = get_field( 'mobilize_map_styles', 'mobilize_event' );
	$mobilize_mode            = get_field( 'mobilize_mode', 'mobilize_event' );
	$mobilize_static_endpoint = get_field( 'mobilize_static_endpoint', 'mobilize_event' );
	$mobilize_virtual_scroll  = get_field( 'mobilize_virtual_scroll', 'mobilize_event' );
	$mobilize_sourcing        = get_field( 'mobilize_sourcing', 'mobilize_event' );
	$mapDefaultBounds         = get_field( 'mobilize_map_bounds', 'mobilize_event' );
	if ( post_type_exists( 'office' ) ) {
		$offices = mobilize_get_offices_static();
	}
	?>
  <div data-widget-host="we-mobilize" class="social-hide" style="min-height: 60vh; display: flex;
align-items: stretch;
position: relative;">
	<div class="wem__loader high">
	  <div class="wem__loader__icon">
		<div style="width:100%;height:100%" class="lds-dual-ring"></div>
	  </div>
	</div>
	<script type="text/props">
	  {
		"base_path": "/<?php echo get_page_uri( get_the_ID() ); ?>",
		"api_key": "<?php echo esc_attr( get_theme_mod( 'google_maps_api_key' ) ); ?>",
		"api_url": "/wp-json",
		"endpoint":"we-mobilize",
		"default_zoom": 4,
		"min_zoom": 4,
		"link_source": <?php echo $mobilize_sourcing ? ( '"' . $mobilize_sourcing . '"' ) : '""'; ?>,
		"title": "<?php echo $title ? $title : get_the_title(); ?>",
		"alt_title": "THANKS! NOW FIND AN EVENT NEAR YOU.",
		"description": "<?php echo $description ? $description : get_the_content(); ?>",
		"api_static": <?php echo $mobilize_mode == 'static' ? ( '"' . $mobilize_static_endpoint . '"' ) : 'false'; ?>,
		"virtual_scroll": <?php echo $mobilize_virtual_scroll ? 'true' : 'false'; ?>,
		"show_images": <?php echo get_field( 'show_mobilize_images', 'mobilize_event' ) !== 'no' ? 'true' : 'false'; ?>,
		"no_events_message":"<?php echo get_field( 'no_events_message', 'mobilize_event' ); ?> ",
		"categories": [
		<?php
		echo join(
			',',
			array_map(
				function( $term ) {
					return '"' . $term->slug . '"';
				},
				get_terms(
					array(
						'taxonomy'   => 'event_types',
						'hide_empty' => false,
					)
				)
			)
		);
		?>
						],
			 <?php
				if ( $mapStyles && false !== json_decode( $mapStyles ) ) :
					?>
		  "mapStyles": <?php echo $mapStyles; ?>,<?php endif; ?>
								  <?php
									if ( $mapDefaultBounds && false !== @json_decode( $mapDefaultBounds ) ) :
										?>
		  "default_bounds": <?php echo $mapDefaultBounds; ?>,<?php endif; ?>
		"extra_map_features": 
		<?php
		if ( post_type_exists( 'office' ) ) {
			echo json_encode( $offices );
		} else {
			echo '[]'; }
		?>
	  }
	</script>
	<script async src="<?php echo plugins_url( '/we-mobilize/frontend/build/bundle.js?2' ); ?>"></script>
  </div>
	<?php
}


?>
